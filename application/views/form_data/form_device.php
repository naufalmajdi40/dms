
<?php 
$machine_code="";
$device_name="";
$gi_name="";
$latitude="";
$longitude="";
$location="";
$status ="add";
$id="";
$ultgId="";
if(isset($data_dms))
	{
		
		//var_dump($data_dms);
		$machine_code=$data_dms[0]->machine_code;
		$device_name=$data_dms[0]->device_name;
		$gi_name=$data_dms[0]->gi_name;
		$latitude=$data_dms[0]->latitude;
		$longitude=$data_dms[0]->longitude;
		$location=$data_dms[0]->location;
		$id=$data_dms[0]->id;
		$ultgId=$data_dms[0]->ultg_id;
		$status="update";
		
	}
?>
<div class="box-body">
<div class="form-group">
	<p hidden id="id"><?php echo $id;?></p>
	<p hidden id="status"><?php echo $status;?></p>
	<label for="machine_code">Machine Code</label>
	<input type="text" class="form-control" id="machine_code" value="<?php echo $machine_code;?>" placeholder="Enter Machine code"> 
</div>
<div>
	<div class="form-group">
	<label for="gi_name">SELECT ULTG</label>
	<!-- <input type="text" class="form-control"  id="gi_name" value="<?php echo $gi_name;?>" placeholder="GI Name"> -->	
	<select class="form-control"  name="ultg" id="ultg">
			<!-- <option value="volvo">Volvo</option>
			<option value="saab">Saab</option>
			<option value="mercedes">Mercedes</option>
			<option value="audi">Audi</option> -->
			<?php
			
				foreach ($data_ultg as $x) {
				$selected="";
				if($x->ultg_id==$ultgId){
					$selected="selected";
				}
				echo '<option value="'.$x->ultg_id.'" '.$selected.'>'.$x->ultg_name.'</option>';
				}
											
			?>

	</select>
</div>
<div>
	<div class="form-group">
	<label for="gi_name">GI Name</label>
	<input type="text" class="form-control"  id="gi_name" value="<?php echo $gi_name;?>" placeholder="GI Name">
</div>
<div class="form-group">
	<label for="device_name">Device Name</label>
	<input type="device_name" class="form-control" id="device_name" value="<?php echo $device_name;?>" placeholder="Device Name">

<div>
	<div class="form-group">
	<label for="latitude">Latitude</label>
	<input type="number" class="form-control"  id="latitude" value="<?php echo $latitude;?>" placeholder="Latitude">
</div>
<div>
	<div class="form-group">
	<label for="longitude">Longitude</label>
	<input type="number" class="form-control"  id="longitude" value="<?php echo $longitude;?>" placeholder="Longitude">
</div>
<div>
	<div class="form-group">
	<label for="Location">Location</label>
	<input type="text" class="form-control"  id="location" value="<?php echo $location;?>" placeholder="location">
</div>
       <button type="button" class="btn btn-danger" onClick="SaveForm()">SAVE</button>
     
</div>


