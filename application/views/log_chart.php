
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

<!-- t?code=${param[0]}&device=${param[1]}&alias=${ -->
<style>
    .amcharts-amexport-item {
  
  background-color: #ffffff;
  
}

</style>
 
<select class="text-black form-control bg-navy text-white" >
  <option value="now"  <?php if($_GET["tipe"]=="now"){echo 'selected';}?>><b>Current Date</b></option>
  <option value="week" <?php if($_GET["tipe"]=="week"){echo 'selected';}?>><b>Last Week</b></option>
  <option value="month" <?php if($_GET["tipe"]=="month"){echo 'selected';}?>><b>Last Month</b></option>
 </select>
<div class="card card-bordered">
    <p hidden id="log"><?php echo json_encode($log)?></p>
    <div class="card-body">
        <div id="kt_amcharts_2" style="height: 500px;"></div>
    </div>
</div>

<script>
am4core.ready(function () {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end
var data = [];
let name=$('#nameDevice').text()
$('select').on('change', function() {
  let code =$('#code').text()
  let device =$('#device').text()
  let alias =$('#alias').text()
  $("#log-body").load(`../../device_relay/log_chart?code=${code}&device=${device}&alias=${alias}&tipe=${this.value}`);
 });

let logshtml = document.getElementById('log').innerHTML
let logs=JSON.parse(logshtml)
for (let i=0;i<logs.length;i++){
    let newDate = Date.parse(new Date(logs[i].tanggal))
    data.push({ Timestamp: newDate, Value: logs[i].val });
}

// Create chart
chart = am4core.create('kt_amcharts_2', am4charts.XYChart);

var price1 = 1000, price2 = 1200;
var quantity = 30000;

chart.data = data;


var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
dateAxis.renderer.grid.template.location = 0;
dateAxis.renderer.labels.template.fill = am4core.color('#FFFFFF');


var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.tooltip.disabled = true;
valueAxis.renderer.labels.template.fill = am4core.color('#FFFFFF');

valueAxis.renderer.minWidth = 60;


var series = chart.series.push(new am4charts.LineSeries());
series.name = 'Data Value';
series.color = 'Data Value';
series.dataFields.dateX = 'Timestamp';
series.dataFields.valueY = 'Value';
series.tooltipText = "{valueY.value}  on {dateX.formatDate('yyyy-MM-ddTHH:mm:ss')}";
series.fill = am4core.color('#FFFFFF');
series.stroke = am4core.color('#00FFFF');
series.strokeWidth = 2;
series.tensionX = 0.77;
series.smoothing = "monotoneX"; // <--- HERE
series.tensionX=1

series.tensionY=1
chart.cursor = new am4charts.XYCursor();

var scrollbarX = new am4charts.XYChartScrollbar();
scrollbarX.series.push(series);
chart.scrollbarX = scrollbarX;

chart.exporting.menu = new am4core.ExportMenu();
//export chart
chart.exporting.menu.align = "left";
chart.exporting.menu.verticalAlign = "top";
// chart.exporting.dateFormat ='yyyy-MM-ddTHH:mm:ss';
chart.exporting.filePrefix = "Log_Monitoring";
chart.exporting.formatOptions.getKey("csv").disabled = true;
chart.exporting.formatOptions.getKey("html").disabled = true;
chart.exporting.title=name
chart.exporting.adapter.add('data', (data) => {
data.data.map(signalObj => {
    let dt=timestampToDate(signalObj.Timestamp)
   
    signalObj.Date = dt
    return signalObj;
}); // this shows date in 12 hour format with AM/PM
return data;
});

// chart.exporting.adapter.add("data", function(data) {
//       data.forEach(function(dataItem) {
//         dataItem.Date = am4core.time.format(dataItem.Date, "dd-MM-yyyy HH:mm:ss");
//       });
//       return data;
//     });



}); // end am4core.ready()
function timestampToDate(timestamp) {
  const date = new Date(timestamp);
  const day = date.getDate().toString().padStart(2, '0');
  const month = (date.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-indexed
  const year = date.getFullYear();
  const hours = date.getHours().toString().padStart(2, '0');
  const minutes = date.getMinutes().toString().padStart(2, '0');
  const seconds = date.getSeconds().toString().padStart(2, '0');

  return `${day}-${month}-${year} ${hours}:${minutes}:${seconds}`;
}
</script>