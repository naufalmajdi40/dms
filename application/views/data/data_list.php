<!-------------------------------------------------------*/
/* Copyright   : Amin Rusydi                             */
/* Hayooooooooooo Ngintip ajah nih.......               */
/*-------------------------------------------------------->
<script type="text/javascript" src="/js/datepicker/js/datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="/js/datepicker/css/base.css" />
<link rel="stylesheet" type="text/css" href="/js/datepicker/css/dark.css" />
<section class="content-header">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>


       <h3 style="margin-top:5px;">
	   	Device Monitoring System
       <small>Bring Live Your Device</small>
      </h3>
      <!--<h3 style="margin-top:-12px; margin-bottom:15px;"><small>smart planting system, for a better life</small></h3>-->
      <ol class="breadcrumb">
        <li><a href="admin"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="active">Disturbance Record Table</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div  class="box bg-navy">        
        <div  class="box-body">
			<center ><legend class="text-white">Disturbance Record Table</legend></center>
  			<br>
			<table id="disturbance" class="table table-bordered  example" style="margin-bottom: 10px;">
			<thead>
				<tr>
					<th>Date</th>
					<th>Time</th>
					<th>GI Name</th>
                    <th>Device</th>
					<th>Relay</th>
					<th>Relay ID</th>
					<th>Rack Location</th>
					<th>Status</th>
					<th>DR Files</th>
					
				</tr>
			</thead>
				<?php
				  $no=1; // Nomor urut dalam menampilkan data
				  $jumlahSks=0; // Jumlah SKS dimulai dari 0
				  $i = 0;
				  // Menampilkan data KRS
				  foreach(array_reverse($sensor_data) as $i => $data ){
					// if ($i++ >= 10) break;
				
					
				?>
				<tr>
					 <td><?php $cobaTanggal = date_create($data->tanggal); echo date_format($cobaTanggal,"Y-m-d"); ?></td>
					 <td><?php echo $data->waktu; ?></td>
					 <td><?php echo $data->gi_name; ?></td>
                     <td><?php if($data->port_type==0){echo "Port Number ".$data->description;} else{echo "IP".$data->description;}  ?></td>
                     <td><?php echo $data->type; ?></td>
					 <td><?php echo $data->relay_id; ?></td>
					 <td><?php echo $data->lokasi; ?></td>
					 <td>
						<?php 
							echo $data->status;
								//  $jumlahSks+=$krs->sks;
						?>
						
					 </td>
					 <td style="text-align:center" width="120px">
					 <button type="button" onclick="location.href='dr_files/<?php echo $data->nama_file; ?>.zip'"  class="btn btn-warning" ><i class="fa fa-download" aria-hidden="true"></i> Download</button>
						
					</td>
				</tr>
				<?php
				 
				} 
		
				?>
				
				</div>  
			  </table >    </div>  
   



<script>
 $(document).ready(function () {
    // Setup - add a text input to each footer cell
    $('#disturbance thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#disturbance thead');
 
    var table = $('#disturbance').DataTable({
         dom: 'Bfrtip',
        buttons: [
          
          {
            extend: 'excel',
             className: 'btn btn-success',
             text: '<h4 style="font-size: 13px;"><i class="fas fa-file-excel fa-x5  text-success"> </i> Export XLS </h4>',
            titleAttr: 'Disturbance Record',
            exportOptions: {
                    columns: [0, 1, 2,3,4, 5,6,7]
                }           
            },
          {
            extend: 'pdf',
                className: 'btn btn-success',
            text: '<h4 style="font-size: 13px;"><i class="fas fa-file-pdf fa-x5 text-danger"> </i> Export PDF </h4>',
            titleAttr: 'Disturbance Record' ,
                exportOptions: {
                    columns: [0, 1, 2,3,4, 5,6,7]
                }            
           
            }
        ],
        orderCellsTop: true,
        fixedHeader: true,
        
        initComplete: function () {
            var api = this.api();
            // For each column
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    // Set the header cell to contain the input element
                   
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    
                    if(title!="DR Files"){
                    
						if(title=="Date"){
							$(cell).html('<input type="date"  style="width:100px;color:black;" placeholder="YYYY/MM/DD"/>');
						}
						else if(title=="Time"){
							$(cell).html('<input type="time"  style="width:100px;color:black;" placeholder="YYYY/MM/DD"/>');
						}
						else{
							$(cell).html('<input type="text" style="width:100px;color:black;" placeholder="' + title + '" />');
						}
					}
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('change', function (e) {
                            // Get the search value
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();
 
                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();
                        })
                        .on('keyup', function (e) {
                            e.stopPropagation();
 
                            $(this).trigger('change');
                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });
});
</script>






	
		

		

		
		

		