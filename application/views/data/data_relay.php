<!-------------------------------------------------------*/
/* Copyright   : Amin Rusydi                             */
/* Hayooooooooooo Ngintip ajah nih.......               */
/*-------------------------------------------------------->
<section class="content-header">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>


       <h3 style="margin-top:5px;">
	   	Device Monitoring System
       <small>Bring Live Your Device</small>
      </h3>
      <!--<h3 style="margin-top:-12px; margin-bottom:15px;"><small>smart planting system, for a better life</small></h3>-->
      <ol class="breadcrumb">
        <li><a href="admin"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="active">Disturbance Record Table</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
		<div class="container-fluid bg-navy">
			<div class="box bg-navy">
      <center><legend class="text-white">Relay List </legend></center>	
            <div class="card card-primary card-outline card-tabs">
              
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-home-tab">
                  <div class="tab-pane active show" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
				<div style="margin:30px;">
					<?php //var_dump($data);?>
					<table id="relay" class="table table-bordered  dataTable dtr-inline" role="grid" aria-describedby="example2_info">
                 
				 <thead>
            <tr role="row">
              <th>machine_code</th>
              <th>id device</th>
              <th >Relay Type</th>
              <th >Protocol</th>
              <th >IP Address</th>
              <th >Rack Location</th>
              <th >Status</th>
              <th hidden style ="width:10%">Action</th>
            </tr>
				   </thead>
				   <tbody>
            <?php
            $no=1;
            foreach($data->result_array() as $i):?>
            <tr>
                <?php
                $port_type="";
                if($i['port_type']=="0"){
                   $port_type="Modbus";
                }
                else if($i['port_type']=="1"){
                   $port_type="TCP /IP";
                }
                else if($i['port_type']=="2"){
                   $port_type="IEC61850";
                }
                ?>
                <th><?php echo $i['machine_code'];?></th>
                <td><?php echo $i['id_device']; ?></a></td>
                <td><?php echo $i['type']; ?></a></td>
                  <td><?php echo $port_type; ?></a></td>
                <td><?php echo $i['ip_address']; ?></a></td>
                <td><?php echo $i['rack_location']; ?></a></td>
                 <td><?php echo $i['kesiapan']; ?></a></td>
                 <td hidden ><button class="btn btn-primary " onClick="save()"><i class="fas fa-cog"></i></button>&nbsp<button class="btn btn-danger "><i class="fas fa-trash"></i></button></td>
                         
            </tr>
              <?php endforeach;?>
					</tbody>
        </table>
					
					
						
					<br>
				</div>
				</div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
				
		</div>
	  
	  <br>
      <div  >        
<script>
  $(document).ready(function () {
    // Setup - add a text input to each footer cell
    $('#relay thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#relay thead');
 
    var table = $('#relay').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        initComplete: function () {
            var api = this.api();
            // For each column
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    // Set the header cell to contain the input element
                   
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    
                    if(title!="Action"){
                      $(cell).html('<input type="text" style="width:150px;color:black;" placeholder="' + title + '" />');
                    }
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('change', function (e) {
                            // Get the search value
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();
 
                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();
                        })
                        .on('keyup', function (e) {
                            e.stopPropagation();
 
                            $(this).trigger('change');
                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });
});
</script>






	
		

		

		
		

		