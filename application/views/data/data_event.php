<!-------------------------------------------------------*/
/* Copyright   : Amin Rusydi                             */
/* Hayooooooooooo Ngintip ajah nih.......               */
/*-------------------------------------------------------->
<section class="content-header">

<style>
.ui-datepicker-title {
	text-align: center;
	line-height: 2rem;
	margin-bottom: 0.25rem;
	font-size: 0.875rem;
	font-weight: 500;
	padding-bottom: 0.25rem;
	color:black;
}
.ui-datepicker-week-col {
	color: #78909C;
	font-weight: 400;
	font-size: 0.75rem;
}
.ui-datepicker-calendar tbody td a {
	display: block;
	border-radius: 0.25rem;
	line-height: 2rem;
	transition: 0.3s all;
	color: #546E7A;
	font-size: 0.875rem;
	text-decoration: none;
}
</style>

       <h3 style="margin-top:5px;">
	   	Device Monitoring System
       <small>Bring Live Your Device</small>
      </h3>
      <!--<h3 style="margin-top:-12px; margin-bottom:15px;"><small>smart planting system, for a better life</small></h3>-->
      <ol class="breadcrumb">
        <li><a href="admin"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="active">Event Record Table</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
	  <div  class="box bg-navy">        
        <div  class="box-body">
			<center><legend  class="text-white"> Events Record Table</legend></center>
			<div class="row card-primary  ">
			<div class="col-md-8">
			
			</div></div>
			<br>
        
			<table id="event" class="table table-bordered " style="margin-bottom: 10px;">
			<thead>
				<tr>
					<th>Date</th>
					<th>Time</th>
					<th>GI Name</th>
                    <!-- <th>Device</th> -->
					<th>Relay Name</th>
					<th>Relay ID</th>
					<th>Rack Location</th>
					<th>Status</th>
					
					
				</tr>
			</thead>
				<?php
				  $no=1; // Nomor urut dalam menampilkan data
				  $jumlahSks=0; // Jumlah SKS dimulai dari 0
				  $i = 0;
				  foreach(array_reverse($sensor_data) as $i => $data ){
					// if ($i++ >= 10) break;
				
					
				?>
				<tr>
					 <td><?php $cobaTanggal = date_create($data->tanggal); echo date_format($cobaTanggal,"Y-m-d"); ?></td>
					 <td><?php echo $data->waktu; ?></td>
					 <td><?php echo $data->gi_name; ?></td>
                     <!-- <td><?php// if($data->port_type==0){echo "Port Number ".$data->description;} else{echo "IP".$data->description;}  ?></td> -->
                     <td><?php echo $data->type; ?></td>
					 <td><?php echo $data->relay_id; ?></td>
					 <td><?php echo $data->lokasi; ?></td>
					 <td>
						<?php 
							echo $data->status;
						?>
					 </td>
					
				</tr>
				<?php
				 
				} 
		
				?>
				
				</div>  
			  </table >    </div>  
   



<script>
	
 $(document).ready(function () {
    // Setup - add a text input to each footer cell
    $('#event thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#event thead');
 
    var table = $('#event').DataTable({
        dom: 'Bfrtip',
        buttons: [
          
          {
            extend: 'excel',
             className: 'btn btn-success',
             text: '<h4 style="font-size: 13px;"><i class="fas fa-file-excel fa-x5  text-success"> </i> Export XLS </h4>',
            titleAttr: 'Event Record'           
            //filename: $fileName
            },
          {
            extend: 'pdf',
                className: 'btn btn-danger',
            text: '<h4 style="font-size: 13px;"><i class="fas fa-file-pdf fa-x5 text-danger"> </i> Export PDF </h4>',
            titleAttr: 'Event Record'           
           // filename: $fileName
            }
        ],

        orderCellsTop: true,
        fixedHeader: true,
       
        initComplete: function () {
            
            var api = this.api();
            // For each column
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    // Set the header cell to contain the input element
                   
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    
                    if(title!="DR Files"){
                    
						if(title=="Date"){
							$(cell).html('<input type="date"  style="width:100px;color:black;" placeholder="YYYY/MM/DD"/>');
						}
						else if(title=="Time"){
							$(cell).html('<input type="time"  style="width:100px;color:black;" placeholder="YYYY/MM/DD"/>');
						}
						else{
							$(cell).html('<input type="text" style="width:100px;color:black;" placeholder="' + title + '" />');
						}
					}
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('change', function (e) {
                            // Get the search value
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();
 
                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();
                        })
                        .on('keyup', function (e) {
                            e.stopPropagation();
 
                            $(this).trigger('change');
                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        
       },
       
    });
});

</script>






	
		

		

		
		

		