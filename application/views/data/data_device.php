<!-------------------------------------------------------*/
/* Copyright   : Amin Rusydi                             */
/* Hayooooooooooo Ngintip ajah nih.......               */
/*-------------------------------------------------------->
<section class="content-header">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>


       <h3 style="margin-top:5px;">
	   	Device Monitoring System
       <small>Bring Live Your Device</small>
      </h3>
      <!--<h3 style="margin-top:-12px; margin-bottom:15px;"><small>smart planting system, for a better life</small></h3>-->
      <ol class="breadcrumb">
        <li><a href="admin"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="active">Disturbance Record Table</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
		<div class="container-fluid">
			<div class="box bg-navy">
      <center><legend class="text-white">DMS list </legend></center>	
            <div class="card card-primary card-outline card-tabs">
              
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-home-tab">
                  <div class="tab-pane active show" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                    <div style="margin:30px;">
                      <?php //var_dump($data);?>
                      <button class="btn btn-danger" onClick="loadForm()" data-toggle="modal" data-target="#myModal">ADD +</button>
                      <br>
                      <br>
                      <table id="device" class="table table-bordered dataTable dtr-inline" role="grid" aria-describedby="example2_info">   
                      <thead>
                        <tr role="row">
                          <th >No</th>
                          <th class="sorting">ULTG Name</th>
                          <th class="sorting">GI Name</th>
                          <th>Device Name</th>	
                          
                          <th class="sorting">Location</th>
                          <th  class="sorting" style ="width:10%">Action</th>
                        </tr>
                        </thead>
                          <tbody>
                            <?php
                            $no=1;
                            foreach($data->result_array() as $i):?>
                            <tr>
                                <th><?php echo $no++;?></th>
                                <th><?php echo $i['ultg_name'];?></th>
                                <td><?php echo $i['gi_name']; ?></a></td>
                                <th><?php echo $i['device_name'];?></th>
                                <td><?php echo $i['location']; ?></a></td>
                                <td >
                                  <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" onClick="loadFormId(<?php echo $i['id']; ?>)"><i class="fas fa-cog"></i></button>&nbsp
                                  <button class="btn btn-danger"  onClick="del_dms(<?php echo $i['id']; ?>)">
                                  <i class="fas fa-trash" ></i></button></td>         
                            </tr>
                              <?php endforeach;?>
                          </tbody>
                    </table>
                    <br>
                  </div>
                </div>
              </div>
            </div>
              <!-- /.card -->
            </div>
          </div>
		</div>
	  <br>
  <div  >        
<!-- //--------------------------------------------------modal------------------------------------------------------------// -->
<div class="container">
  <!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-navy" >
          <h4 class="modal-title"></h4>
          <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body bg-navy" id="modal-body">
       
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer bg-navy">
        </div>
        
      </div>
    </div>
  </div>
</div>
<!-----------------------------------------------------end modal---------------------------------------------------------- -->
<script>
  function del_dms(x){
    
    let postData = {
          id: x,
      };
     $.ajax({
            type: "POST",
            url: "../device/del_dms", // Replace with your server-side script URL
            contentType: 'application/x-www-form-urlencoded', // Set content type to JSON
            data: postData, // Specify the data to be sent in the request body
            success: function(response){
                console.log("Request successful!");
                console.log("Response from server: " + response);
                alert(response)
                window.location.reload();
            },
            error: function(xhr, status, error){
                // Handle error
                console.error("Error making request: " + error);
            }
        });
  }
  function loadForm(){
     $("#modal-body").load("dms/load_form_device"); 
    }
    function loadFormId(x){
        url="dms/load_form_device_id?id="+x
        $("#modal-body").load(url); 
    }
  function SaveForm(){
    let machine_code = $('#machine_code').val()
    let device_name = $('#device_name').val()
    let gi_name =$('#gi_name').val()
    let latitude =$('#latitude').val()
    let longitude =$('#longitude').val()
    let location =$('#location').val()
    let id =$('#id').text()
    let ultg_id =$('#ultg').find(":selected").val();
    if(machine_code.length<1 ||device_name.length<1 ||gi_name.length<1 ||latitude.length<1 ||longitude.length<1){
        alert("Form is Empty")
        return false
    }
    let postData = {
          machine_code: machine_code,
          device_name: device_name,
          gi_name :gi_name,
          latitude:latitude,
          longitude:longitude,
          location:location,
          ultg_id:ultg_id,

          id:id
      };
    url =""
    let status = $('#status').text()
   
    if(status == "add"){
      url = "dms/save_dms"
    }
    else if(status=="update"){
      url = "dms/update_dms"
    }
   
    $.ajax({
            type: "POST",
            url:url, // Replace with your server-side script URL
            contentType: 'application/x-www-form-urlencoded', // Set content type to JSON
            data: postData, // Specify the data to be sent in the request body
            success: function(response){
                // Handle success response
                console.log("Request successful!");
                console.log("Response from server: " + response);
                alert(response)
                window.location.reload();
            },
            error: function(xhr, status, error){
                // Handle error
                console.error("Error making request: " + error);
            }
        });
   
    
  }
 $(document).ready(function () {
    // Setup - add a text input to each footer cell
    
    $('#device thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#device thead');
 
    var table = $('#device').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        initComplete: function () {
            var api = this.api();
            // For each column
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    // Set the header cell to contain the input element
                   
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    
                    if(title!="Action" && title!="No"){
                      $(cell).html('<input type="text" style="width:100%;color:black;" placeholder="' + title + '" />');
                    }
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('change', function (e) {
                            // Get the search value
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();
 
                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();
                        })
                        .on('keyup', function (e) {
                            e.stopPropagation();
 
                            $(this).trigger('change');
                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });
});
</script>






	
		

		

		
		

		