<!-------------------------------------------------------*/
/* Copyright   : Amin Rusydi                             */
/* Hayooooooooooo Ngintip ajah nih.......               */
/*-------------------------------------------------------->
<section class="content-header">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
       <h3 style="margin-top:5px;">
	   	Device Monitoring System
       <small>Bring Live Your Device</small>
      </h3>
      <!--<h3 style="margin-top:-12px; margin-bottom:15px;"><small>smart planting system, for a better life</small></h3>-->
      <ol class="breadcrumb">
        <li><a href="admin"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="active">ULTG List</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
		<div class="container-fluid bg-navy">
			<div class="box bg-navy">
      <center><legend class="text-white">ULTG List </legend></center>	
            <div class="card card-primary card-outline card-tabs">
              
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-home-tab">
                  <div class="tab-pane active show" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
				<div style="margin:30px;">
				<button class="btn btn-danger" onClick="loadForm()" data-toggle="modal" data-target="#myModal">ADD +</button>
                      <br>
                      <br>
					<table id="relay" class="table table-bordered  dataTable dtr-inline" role="grid" aria-describedby="example2_info">
                 
				 <thead>
            <tr role="row">
              <th style="width:30px;">No</th>
              <th>ULTG Name</th>
              <th >ULTG Location</th>
              <th >Chat ID</th>
              
              <th style ="width:10%">Action</th>
            </tr>
				   </thead>
				   <tbody>
            <?php
            $no=1;
            foreach($data->result_array() as $i):?>
            <tr>
                
                
                <td ><?php echo $no ?></a></td>
                <td><?php echo $i['ultg_name']; ?></a></td>
                <td><?php echo $i['ultg_loc']; ?></a></td>
                <td><?php echo $i['chat_id']; ?></a></td>
               
                 <td ><button class="btn btn-primary " onClick="loadFormId('<?=$i['ultg_id'];?>')"  data-toggle="modal" data-target="#myModal"> <i class="fas fa-cog"></i></button>&nbsp<button class="btn btn-danger " onClick="del_ultg('<?=$i['ultg_id'];?>')"><i class="fas fa-trash"></i></button></td>
                         
            </tr>
            
              <?php $no++;
            endforeach;?>
					</tbody>
        </table>
					
					
				<!-- //--------------------------------------------------modal------------------------------------------------------------// -->
        <div class="container">
          <!-- The Modal -->
          <div class="modal" id="myModal">
            <div class="modal-dialog">
              <div class="modal-content">
              
                <!-- Modal Header -->
                <div class="modal-header bg-navy" >
                  <h4 class="modal-title"></h4>
                  <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body bg-navy" id="modal-body">
              
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer bg-navy">
                </div>
                
              </div>
            </div>
          </div>
        </div>
<!-----------------------------------------------------end modal---------------------------------------------------------- -->
				</div>
				</div>
                </div>
              </div>
              <!-- /.card -->
      
       
				
	    
<script>
  $(document).ready(function () {
    // Setup - add a text input to each footer cell
    $('#relay thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#relay thead');
 
    var table = $('#relay').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        initComplete: function () {
            var api = this.api();
            // For each column
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    // Set the header cell to contain the input element
                   
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    
                    if(title!="Action"){
                      $(cell).html('<input type="text" style="width:100%;color:black;" placeholder="' + title + '" />');
                    }
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('change', function (e) {
                            // Get the search value
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();
 
                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();
                        })
                        .on('keyup', function (e) {
                            e.stopPropagation();
 
                            $(this).trigger('change');
                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });
});
function loadForm(){
     $("#modal-body").load("ultg/load_form_ultg"); 
    }
  function loadFormId(x){
      url="ultg/load_form_ultg_byid?ultg_id="+x
      $("#modal-body").load(url); 
  }
function SaveForm(){
    let ultg_id = $('#ultg_id').text()
    let ultg_name = $('#ultg_name').val()
    let ultg_loc =$('#ultg_loc').val()
    let chat_id =$('#chat_id').val()
  
    if(ultg_name.length<1 ||ultg_loc.length<1 ||chat_id.length<1){
        alert("Form is Empty")
        return false
    }
    let postData = {
          ultg_id: ultg_id,
          ultg_name: ultg_name,
          ultg_loc :ultg_loc,
          chat_id:chat_id,
         
      };
    url =""
    let status = $('#status').text()
   
    if(status == "add"){
      url = "ultg/save_ultg"
    }
    else if(status=="update"){
      url = "ultg/update_ultg"
    }
   
    $.ajax({
            type: "POST",
            url:url, // Replace with your server-side script URL
            contentType: 'application/x-www-form-urlencoded', // Set content type to JSON
            data: postData, // Specify the data to be sent in the request body
            success: function(response){
                // Handle success response
                console.log("Request successful!");
                console.log("Response from server: " + response);
                if(response =="Data berhasil disimpan"){
                    Swal.fire({
                          position: "center",
                          icon: "success",
                          title: "Data has been saved",
                          showConfirmButton: false,
                          timer: 1500
                        }).then(()=>{
                          window.location.reload();
                        })
                    //window.location.reload();
                }
                else{
                  alert(response)
                }
                //window.location.reload();
            },
            error: function(xhr, status, error){
                // Handle error
                console.error("Error making request: " + error);
            }
        });
      }

  function del_ultg(x){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger"
      },
      buttonsStyling: false
    });
    swalWithBootstrapButtons.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
         prosess_del(x)
        swalWithBootstrapButtons.fire({
          title: "Deleted!",
          text: "Your file has been deleted.",
          icon: "success"
        });
         window.location.reload();
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire({
          title: "Cancelled",
          text: "Your imaginary file is safe :)",
          icon: "error"
        });
      }
    });
    
  
  }
  function prosess_del(x){
    let postData = {
          ultg_id: x,
      };
     $.ajax({
            type: "POST",
            url: "ultg/del_ultg", // Replace with your server-side script URL
            contentType: 'application/x-www-form-urlencoded', // Set content type to JSON
            data: postData, // Specify the data to be sent in the request body
            success: function(response){
                console.log("Request successful!");
                console.log("Response from server: " + response);
               
               // window.location.reload();
            },
            error: function(xhr, status, error){
                // Handle error
                console.error("Error making request: " + error);
            }
        });
  }
</script>






	
		

		

		
		

		