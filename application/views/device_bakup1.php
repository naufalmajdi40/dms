<!--------------------------------- ----------------------*/
/* Copyright   : Amin Rusydi                            */
/*-------------------------------------------------------->
<?php
	ini_set('display_errors', '0');
    ini_set('error_reporting', E_ALL);
?>
  
    <!-- Content Header (Page header) -->

    <section class="content-header">

<!-- <script src="<?php //echo base_url('assets/bower_components/grafik/jquery-3.4.0.min.js')?>"> defer</script>    
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script> -->

 
<script type="text/javascript">
    setInterval(cuacaUpdate,2000); 
    function cuacaUpdate(){
      $('#refreshh').load(location.href + ' #updateTable');
    }
  </script> 

<script type="text/javascript">

    // TODO ada fungsi yang error, dan menyebabkan beberapa fungsi yang lain tidak jalan
    setInterval(scriptUpdate,2000); 
    function scriptUpdate(){
      $('#refresh').load(location.href + ' #update');
     // $kode_mesin =$('#machine_code').text()
      // $('#refreshh').load(location.href + ' #updatee');
      // $('#refreshGambar').load(location.href + ' #updateGambar');
      // $("#responsecontainer").load("<?php echo base_url().'chart/index/'.$kode_mesin ?>");
       $("#pie").load("<?php echo base_url().'pie'; ?>");
    }
  </script>
      <h3 style="margin-top:5px; font-size:18.5px">
	     	Device Monitoring System
       <small>Bring Live Your Device</small>
      </h3>
     

      <ol class="breadcrumb">
        <li><a href="admin"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
       <?php if($login=='T'){ ?>
	
			 <div class="row">
        <div class="col-sm-8">
             <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-info-circle"></i>  Dear <?php echo $username ?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
	    <ul>
              <li> Sepertinya Anda belum mengganti password pada menu <b>user</b>, demi keamanan maka perbarui password Anda. </li>
	      <li> Jangan lupa lengkapi data pribadi Anda pada menu <b>Data Mahasiswa.</b> </li>
	    <ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        
      </div>
      <!-- /.row -->   <?php } ?>
      
    
      

<!-- =========================================================================================================== -->
 <!-- Info boxes -->
<!-- <div class="box-header with-border" style="background-color:#ffffff; border-radius:10px; margin-bottom: 10px;">
			  <h3 class="box-title"><i class="fa fa-area-chart"></i>  Monitoring Panel</h3>
			</div> -->

 

 <div class="clearfix visible-sm-block"></div>

 <div  class="row">
           <!-- /.col (LEFT) -->
        <div  class="col-md-12" hidden>
          <!-- LINE CHART -->
          <div class="box box-info bg-navy">
            <div class="box-header with-border">
              <h3 class="box-title text-white">Coverage Area</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                      <?php 
                  $kodePemetaan= array();
                  $kodeHasil =  array();
                  foreach($pemetaan as $value){
                    array_push($kodePemetaan, $value->machine_code);
                  }

                  foreach($countDisturbance as $dataajahh){
                    array_push($kodeHasil, $dataajahh->machine_code);
                  }

                  $result=array_diff($kodePemetaan,$kodeHasil);

              
              ?>
              <div id="map" style="height:450px"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
<!--     /////////////////////////////////////filter cuy////////////////////////// -->
    <p hidden id="monitorData"><?php echo JSON_encode( $monitor);?></p>
    <p hidden id="id_device"><?php echo $device[0]->id_device;?></p>
    
    <p hidden id="machine_code"><?php echo $device[0]->machine_code;?></p>
           <!-- //////////////////////data monitor cuy/////////////// -->
       
    <?php   for  ($i =0 ;$i<sizeof($device);$i++){
      $type="";
      if($device[$i]->port_type=="0"){
          $type="Modbus";
        }
        else if($device[$i]->port_type=="1"){
          $type="TCP-IP";
        }
        if($device[$i]->port_type=="2"){
          $type="IEC61850";
       }
       ?>
    <div class="col-md-12 ">
      <div class="box box-primary bg-navy " >
        <div class="box-header with-border">
          <h3 class="box-title text-white">
           
            <b> Measurement &nbsp<?php echo($device[$i]->rack_location);?></b>, <?=($device[$i]->type);?>-<?php echo $type;?> </h3>
          <div class="box-tools pull-right" style="display:contents;">
            <button type="button" class="btn btn-box-tool" style="float:right;margin-right: 0.5em;" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <div class="input-group col-md-5" style="float:right;margin-right: 1em;">
            <p hidden id="tipe"><?php echo $tipe?></p>
            <input type="text" id="filter_device" class="form-control" placeholder="Search device">
              <span class="input-group-btn">
                <button type="submit" name="search" onclick="filterdata()" id="search-btn" class="btn btn-primary"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
            
          </div>
        </div>
        <div class="box-body info-box  drag  bg-navy  " id="bd_<?php echo($device[$i]->id_device);?>" style="display:flex;flex-wrap:wrap;flex-direction: row;padding-top:10px;padding-left:10px;" >
      
      </div>   
      </div>
    </div>
      <?php } ?>
      <!-- --------------------------------------------------------------------------------------------------------------- -->
      <?php   for  ($i =0 ;$i<sizeof($device);$i++){
      $type="";
      if($device[$i]->port_type=="0"){
          $type="Modbus";
        }
        else if($device[$i]->port_type=="1"){
          $type="TCP-IP";
        }
        if($device[$i]->port_type=="2"){
          $type="IEC61850";
       }
       ?>
    <div class="col-md-12 ">
      <div class="box box-primary bg-navy " >
        <div class="box-header with-border">
          <h3 class="box-title text-white">
          <b> Event &nbsp<?php echo($device[$i]->rack_location);?></b>, <?=($device[$i]->type);?>-<?php echo $type;?> </h3>
          <div class="box-tools pull-right" style="display:contents;">
            <button type="button" class="btn btn-box-tool" style="float:right;margin-right: 0.5em;" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
           
            
          </div>
        </div>
        <div class="box-body info-box  drag  bg-navy  " id="bd_sts_<?php echo($device[$i]->id_device);?>" style="display:flex;flex-wrap:wrap;flex-direction: row;padding-top:10px;padding-left:10px;" >
      
      </div>   
      </div>
    </div>
      <?php } ?>
     

      <!-- -----------------------------end  data monitor disini---------------------------------------     -->
 
  <div <?php echo $menu ;?> class="row">
    <div class="col-md-12 ">
      <div class="box">
        <div class="box-body bg-navy">
          <div class="row ">
            <div class="col-md-5 ">
              <p class="text-center">
                <strong>Data Comparison </strong></p>
                <p id="dataGabung1" hidden><?php echo json_encode($gabung1);?></p>
                <?php 
                
                $i=0;
                $color=["aqua","red","yellow","green"];
                foreach($gabung1 as $value){
               // echo $value->item_id;
                ?>
              <div class="progress-group">
                <span class="progress-text"><?php echo $value->name;?></span>
                <span class="progress-number ">/<?php echo intval($value->max_value);?></span><span class="progress-number <?php echo "txtcomp_".$value->machine_code."_".$value->id_device."_".$value->alias;?>"><b>0</b></span>
                <div class="progress sm">
                <!-- dt_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias} -->
                  <div class=" <?php echo "comp_".$value->machine_code."_".$value->id_device."_".$value->alias;?> progress-bar progress-bar-<?php echo $color[$i]?>" style="width: 2%"></div>
                </div>
              </div>
            <?php $i++;} ?>
            </div><!-- /.col -->
          <div class="col-md-7">
              <p class="text-center">
                <strong>Data Comparison Graph </strong>
             
              </p>
              <button class="btn btn-danger float-left" onClick="openConfig('<?php echo $kode_mesin ;?>')"><i class="fas fa-cog"></i></button>
               <div class="chart" ><!-- style="width: 200px;" -->
                <!-- Sales Chart Canvas -->
                <canvas id="recapDevice" style="height: 180px;"></canvas>
              </div><!-- /.chart-responsive -->
            </div><!-- /.col -->
            
          </div><!-- /.row -->
        </div><!-- ./box-body -->
      </div>
    </div>
    </div>

<!-- =========================================graph================================================================== -->
<p hidden id="graph_current"><?php echo json_encode($cur_graph);?></p>
<p hidden id="graph_volt"><?php echo json_encode($volt_graph);?></p>
<p hidden id="graph_freq"><?php echo json_encode($freq_graph);?></p>

<?php $chart_name=array("Current graph","Voltage graph","freq graph");?>
<div class="row ">
 
<?php for( $t=0;$t<3;$t++){?>
  <div  <?php echo $menu ;?> class="col-md-4 text-white">          
    <div class="box box-info bg-navy">
      <div class="box-header with-border">
        <h3 class="box-title text-white"><?php print_r($chart_name[$t]);?></h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        
        <div class="chart">
          <canvas id="lineChart<?=$t?>" style="height: 180px;"></canvas>

          </ul>
        </div>
      </div>
    </div>
  </div>
  <?php }?>
</div>        

<!-- ==============================================end graph========================================================= -->


        
   
	  <div class="box-footer bg-navy" style="text-align:center">
         DMS &copy<a href=""><strong> PT. Micronet Gigatech Indoglobal</strong></a> 2021
    </div>

<!-- =========================================================================================================== -->


  

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer bg-navy" >
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <br>
    <!-- <strong></strong> -->
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<div class="modal fade" tabindex="-1" role="dialog" id="logDevice">
  <div class="modal-dialog modal-lg modalcfg">
    <div class="modal-content  bg-navy">
      <div class="modal-header">
        <button  type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-tachometer"></i> History Device <span id="nameDevice"></span></h4>
        <p hidden id="code"></p>
    <p hidden  id="device"></p>
    <p hidden id="alias"></p>
      </div>
      <div class="modal-body" >

        <div class="box-body modal-box" style="height:500px; overflow: auto ;">
          <div hidden id="interactive" style="width:100%;height: 300px;"></div>
          <div id="log-body" style="width:100%;height: 300px;"></div>
        </div>

      </div>
      <div class="modal-footer">
       
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- jQuery 3 -->
<!-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script> -->
<script src="<?php echo base_url('assets/bower_components/jquery/dist/jquery.min.js')?>"></script>

<script src="<?php echo base_url('assets/bower_components/jquery/dist/jquery-ui.js')?>"></script>
<!-- <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script> -->

<!-- Bootstrap 3.3.7 -->
<script src="<?php  echo base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!--  SlimScroll -->
<script src="<?php //echo base_url('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick
<script src="<?php //echo base_url('assets/bower_components/fastclick/lib/fastclick.js')?>"></script> -->

<!-- 
<script src="assets/bower_components/select2/dist/js/select2.full.min.js"></script> -->
<!-- InputMask -->
<!-- <script src="assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="assets/plugins/input-mask/jquery.inputmask.extensions.js"></script> -->

<!-- <script src="<?php echo base_url('assets/bower_components/grafik/jquery-3.4.0.min.js')?>"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/paho-mqtt/1.0.2/mqttws31.min.js"></script>
<script src="<?php echo base_url('assets/js/data-mqtt.js') ?>"></script>
<!-- <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script> -->
 <!-- <script src="<?php echo base_url('assets/bower_components/chart.js/Chart.min.js')?>"></script> -->
 <script src="<?= base_url('assets/plugins/chart.js/chart.umd.js')?>"></script>
 
<script src="<?php  echo base_url('assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')?>"></script>

<!--<script src="<?php echo base_url('assets/bower_components/grafik/jquery-3.4.0.min.js')?>"></script>
 <script src="<?php echo base_url('assets/bower_components/grafik/mdb.min.js')?>"></script>  -->
<script src="<?php echo base_url('assets/plugins/knob/jquery.knob.js')?>"></script>
<!-- <script src="<?php echo base_url('assets/plugins/sparkline/jquery.sparkline.min.js')?>"></script> -->

<script src="<?php echo base_url('assets/js/adminlte.min.js') ?>"></script> 

<!-- FLOT CHARTS -->
<script src="<?= base_url('assets/plugins/flot/jquery.flot.min.js') ?>"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="<?= base_url('assets/plugins/flot/jquery.flot.resize.min.js') ?>"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="<?= base_url('assets/plugins/flot/jquery.flot.pie.min.js') ?>"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="<?= base_url('assets/plugins/flot/jquery.flot.categories.min.js') ?>"></script>
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>


<!-- AdminLTE for demo purposes -->
<!-- AdminLTE App -->

<script>
 // localStorage.clear();
  var HubIcon = L.Icon.extend({
    options: {
       iconSize:     [60, 60],
       iconAnchor:   [22, 39],
       shadowAnchor: [4, 62],
       popupAnchor:  [-3, -40]
    }
});
var greenIcon = new HubIcon({
    iconUrl: '../../assets/img/switch1.png'
});

var redIcon = new HubIcon({
    iconUrl: '../../assets/img/switch1.png'
});

	var map = L.map('map').setView( [-6.200000, 106.816666], 9);
  var customOptions =
        {
        'maxWidth': '680px',
        'className' : 'custom'
        };

   L.tileLayer('https://tile.jawg.io/jawg-dark/{z}/{x}/{y}.png?access-token=cRC4wKMYnFlJ9D9JFobqKUFgIOpLiWSpAo8Ydjn524LpMUD29WNyjR6s5EJjL1Gf', {
		maxZoom: 18,
		id: 'mapbox/light-v10',
	}).addTo(map);


	<?php date_default_timezone_set('Asia/Jakarta');
		$tanggal= date('Y-m-d'); 
  
    foreach ($pemetaan as $value) { 
      foreach($result as $a=>$hasil){
       if($value->machine_code == $hasil){
         $find=0;
       }
      }
      echo $value->latitude;  
      foreach($countDisturbance as $codeDisturbance){ 
        if($codeDisturbance->machine_code == $value->machine_code){
          $find = $codeDisturbance->jumlah;}
          } 
           
        if($find>0){$image="redIcon";}else{$image="greenIcon";}?>

var customPopup = "<div style='width:300px;' class='card w-75'><div class='card-body'><h5 class='card-title'>"+<?= '"'.$value->device_name.'"' ?>+"</h5><ul class='list-group list-group-flush'><li class='list-group-item'>Machine Code<span class='badge bg-primary rounded-pill'>"+<?= '"'.$value->machine_code.'"' ?>+"</span></li><li class='list-group-item'>Location<span class='badge bg-primary rounded-pill'>"+<?= '"'.$value->location.'"' ?>+"</span></li><li class='list-group-item'>Disturbance Today : <span class='badge bg-primary rounded-pill'>"+<?= '"'.$find.'"' ?>+"</span></li></ul><a style='color:white;' href='#' class='btn btn-primary'>Detail</a></div></div>";
		L.marker([<?= $value->latitude ?>, <?= $value->longitude ?>], {
        icon: <?= $image ?>,
				radius: 90000,
				color: 'red',
				fillColor: 'red',
				fillOpacity: 0.5,
			}).bindPopup(customPopup,customOptions)
			.addTo(map);

	<?php }  ?>
  function appendWidget(data){
    buf = data
    for(let i =0 ;i<buf.length;i++){
        isNum = false;
        type=""
        port_type=""
        idb = ""
        if(buf[i].port_type=="0"){
          port_type="Modbus";
        }
        else if(buf[i].port_type=="1"){
          port_type="TCP-IP";
        }
        if(buf[i].port_type=="2"){
          port_type="IEC61850";
        }
        let unit=""
        if(buf[i].data_type=="boolean"){
          isNum = true;
          idb = `id="dt_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}"`;
          // type=`<div id="dt_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}" class="lampu l-second" style="width: 1.6em;height: 1.6em;margin-top: 0.1em;"/>`
        }
        else if (buf[i].data_type=="integer" || buf[i].data_type=="float"){
          unit= buf[i].unit
          isNum = true
          // type = `<input type="text" class="knob${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}" value="10" data-skin="tron" data-thickness="0.2" data-angleArc="250" data-angleOffset="-125" data-width="80" data-height="80" data-fgColor="#f1f1f1" data-readonly="false"> <i hidden style="font-size:18px;position:relative;z-index:99;left:17px;top:-126px">%</i>`   
        }
        else if (buf[i].data_type=="persen"){
      
          unit= buf[i].unit
          type = `<input type="text" class="knob${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}" value="10" data-skin="tron" data-thickness="0.2" data-angleArc="250" data-angleOffset="-125" data-width="80" data-height="80" data-fgColor="#f1f1f1" data-readonly="false"> <i hidden style="font-size:18px;position:relative;z-index:99;left:17px;top:-126px">%</i>`   
        }
        else if(buf[i].data_type=="utc-time"){
            type= `<div id="dt_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}"  class="	text-primary" style="width: 50px;height: 50px;margin-top:-20px;margin-left:10px;"><i class="far fa-clock " ></i>`
          }
        else if(buf[i].data_type=="visible-string"){
            type=  `<div id="dt_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}"  class="	text-success" style="width: 50px;height: 50px;margin-top:-20px;margin-left:10px;"><i class="fas fa-info " ></i>`
          }
        dthtml=`
        <div class="" id="${buf[i].machine_code}_${buf[i].im_mon_id}" data-name="${buf[i].name}" data-type="${buf[i].type}" data-port="${port_type}" data-id="${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}" onclick="openHistory(this)"  style="width:240px;margin-right:7px;margin-bottom:-7px;cursor:pointer">
          <div ${idb} class="info-box bg-navy-dark" style="">
            <span class="info-box-icon" style="padding-top:0.1em; padding-left:0px;background: rgb(119 119 119 / 30%) !important;${ isNum?'display:none;':''}">
              ${type} 
            </span>`
        if(isNum)
        {
          dthtml += `<div class="info-box-content" style="position: relative;text-align: center;margin-left: 0px; onclick="openHistory(this)"">
            <p class="info-box-text" id="max_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}" style="display:none;">${buf[i].max_value}</p>
              <span class="info-box-text">${buf[i].name}</span>
              <span class="">${buf[i].type}</span> <span class="">${port_type} </span><br>
              <p hidden class="dvdr_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}">${(buf[i].divider)}</p>
              <span class="info-box-number" style="display:inline;font-size: 28px" id="dtx_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}">0</span><span class="info-box-number" style="display:inline">${unit}</span>
              <span id="linespan${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}" data-name="${buf[i].name}" data-type="${buf[i].type}" data-port="${port_type}" data-id="${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}" onclick="openHistory(this)" style="position: absolute;right: 0.5em;bottom: -0.8em;cursor: pointer;display:none"></span>
            </div>
          </div>
        </div>`
        }
        else
        {
          dthtml += `<div class="info-box-content" style="position: relative;onclick="openHistory(this)">
            <p class="info-box-text" id="max_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}" style="display:none;">${buf[i].max_value}</p>
              <span class="info-box-text">${buf[i].name}</span>
              <span class="">${buf[i].type}</span> <span class="">${port_type} </span><br>
              <span class="info-box-number" style="display:inline" id="dtx_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}">0</span><span class="info-box-number" style="display:inline">${unit}</span>
              <span id="linespan${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}" style="position: absolute;right: 0.5em;bottom: -0.8em;cursor: pointer;display:none"></span>
            </div>
          </div>
        </div>`
        }
            
        if ((buf[i].data_type=="boolean") ){
           $(`#bd_sts_${buf[i].id_device}`).append(dthtml) 
          }
        else{
           $(`#bd_${buf[i].id_device}`).append(dthtml) 
          }
        if ((buf[i].data_type=="persen") ){
          launchKnobTron(`${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}`);
          doLineSpan(`${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}`)
          //dirubah 1
        }


    }

  }
  function filterdata(){
      let filter= $('#filter_device').val()
      let machine_code= $('#machine_code').text()
      let url =`../search_data?name=${filter}&code=${machine_code}`
      console.log(url)
      $.get( url, function( data ) {
         $('#monitorData').text(data)
        $('.drag').html("")
        console.log(data)
        dt=JSON.parse(data)
        appendWidget(dt)

      });
    }

  function openHistory(obj)
  {
    let name = $(obj).attr("data-name");
    let type = $(obj).attr("data-type");
    let port = $(obj).attr("data-port");
    let param =$(obj).attr("data-id").split("_")
  
    $("#log-body").empty();
  
    $(".modal-title").text("History Device "+ name.toUpperCase());
    $('.modalcfg').removeClass("modal-sm")
    $('.modalcfg').addClass("modal-lg")
    $('.modal-box').attr('style','height:500px !important');
    $("#log-body").load(`../../device_relay/log_chart?code=${param[0]}&device=${param[1]}&alias=${param[2]}&tipe=now`);
    // console.log(`../../device_relay/log_chart?code=${param[0]}&device=${param[1]}&alias=${param[2]}`)
    $('#code').text(param[0])
    $('#device').text(param[1])
    $('#alias').text(param[2])
    $("#logDevice").modal("show");
   
  }
  function openConfig(x){
    
    $("#log-body").empty();
      $('.modalcfg').removeClass("modal-lg")
      $('.modalcfg').addClass("modal-sm")
      $(".modal-title").text("Select Comparison Data");
      $("#log-body").load(`../setting_comp?code=${x}`);
      
      $('.modal-box').attr('style','height:300px !important');
      $("#logDevice").modal("show");
    }

  $(document).ready(function () {
    startConnect()  
    //$('.sidebar-menu').tree()
    chartHistory()
    readDevice()
   
   
    function readDevice(){
      $.get("../../mon/apiReadMon",(res)=>{
       
        // $(`#mon_data`).val(res)
        dt = JSON.parse(res)
        buf= dt.data
          appendWidget(buf)
         
          // drawMouseSpeedDemo()
      })
    }


    $(".drag").sortable({
      update: function(event, ui) {           
        let ids = $(this).children().get().map(function(el) {
        distance: 5
          return el.id
            }).join(",");
            console.log(ids)
            $.get( "../updPosition2?data="+ids)
                .done(function(data) {  
                  $.get( "../../mon/updPosition2?data="+ids)
                      .done(function(data) {  
                      console.log(data)
                  });
                  console.log(ids)
              });
              
          }
     });
    
  })

  // Start History Chart in Modal //
  /*
    * Flot Interactive Chart
    * -----------------------
    */
  // We use an inline data source in the example, usually data would
  // be fetched from a server
  var data = [];
  function getRandomData(totalPoints) {

    if (data.length > 0)
      data = data.slice(1);

    // Do a random walk
    while (data.length < totalPoints) {

      var prev = data.length > 0 ? data[data.length - 1] : 50,
              y = prev + Math.random() * 10 - 5;

      if (y < 0) {
        y = 0;
      } else if (y > 100) {
        y = 100;
      }

      data.push(y);
    }

    // Zip the generated y values with the x values
    var res = [];
    for (var i = 0; i < data.length; ++i) {
      res.push([i, data[i]]);
    }

    return res;
  }

  function getChartRandomData(totalPoints) {
    let dt = [];
    while (dt.length < totalPoints) {
      dt.push(Math.round((Math.random()*100)));
    }
    return dt;
  }

  function chartHistory()
  {
    var interactive_plot = $.plot("#interactive", [0], {
      grid: {
        borderColor: "#f3f3f3",
        borderWidth: 1,
        tickColor: "#f3f3f3"
      },
      series: {
        shadowSize: 0, // Drawing is faster without shadows
        color: "#3c8dbc"
      },
      lines: {
        fill: true, //Converts the line chart to area chart
        color: "#3c8dbc"
      },
      yaxis: {
        min: 0,
        max: 100,
        show: true
      },
      xaxis: {
        show: true
      }
    });

    var updateInterval = 500; //Fetch data ever x milliseconds
    var realtime = "on"; //If == to on then fetch data every x seconds. else stop fetching
    function update() {

      interactive_plot.setData([getRandomData(100)]);

      // Since the axes don't change, we don't need to call plot.setupGrid()
      interactive_plot.draw();
      if (realtime === "on")
        setTimeout(update, updateInterval);
    }
    update();
  }

  // Start Sparkline //
  let minterval = 500; // update display every 500ms
  let refreshTime = 1000; // update display every 500ms
  let mousetravel = 0;
  let mpoints_max = 30;
  let mpoints = [];
  let listMPoints = [];
  let pps;

  function doLineSpan(idObj)
  {
    let jsonVal=localStorage.getItem(idObj);
    let dataVal =JSON.parse(jsonVal)
      
    if(listMPoints[idObj] != null)
    { mpoints = listMPoints[idObj]; }
    else
    { mpoints = []; }
    let pps=0
    try {
        pps = dataVal.val;

    } catch (error) {
      pps=0      
    }
   
   
  
    //console.log(pps)
    mpoints.push(pps);
    
    if (mpoints.length > mpoints_max)
    { mpoints.splice(0, 1); }
    listMPoints[idObj] = mpoints;

    try
    {  
  
      $(".knob"+idObj).val(0);
      try {
        let max = parseFloat($(`#max_${idObj}`).text()) ||0 
        let val1=parseFloat(dataVal.val).toFixed(2)
        let dtVal= parseFloat(dataVal.val)*100/max
        
        $(".knob"+idObj).val(dtVal );
        $(".comp_"+idObj).css(`width`,`${dtVal}`);
        $(".txtcomp_"+idObj).text(`${val1}/${max}`);
     //   console.log(max)

        console.log(dtval)
      
      } catch (error) {
        //$(".knob"+idObj).val(0);
       // console.error();
      }
        
      
      $(".knob"+idObj).trigger("change");
      //data berubah 
      
      $("#linespan"+idObj).sparkline(mpoints, {width: '60px', height: '25px', 
        tooltipSuffix: ' pixels per second',
        lineColor: '#009abf', fillColor: '#effbfb',
        minSpotColor: false, maxSpotColor: false, 
        spotColor: '#77f', spotRadius: 1});  
    }
    catch(err)
    { console.log(" Sparkline Error: "+err); }
    
    setTimeout(doLineSpan.bind(null, idObj), refreshTime, idObj); 
  }

  // End Sparkline //

  function launchKnobTron(idObj)
  {
    /* jQueryKnob */

    $(".knob"+idObj).knob({
      'format' : function (value) {
     return value + '%';
  },
        /*change : function (value) {
          //console.log("change : " + value);
          },
          release : function (value) {
          console.log("release : " + value);
          },
          cancel : function () {
          console.log("cancel : " + this.value);
          },*/
        draw: function () {

          // "tron" case
          if (this.$.data('skin') == 'tron') {

            var a = this.angle(this.cv)  // Angle
                    , sa = this.startAngle          // Previous start angle
                    , sat = this.startAngle         // Start angle
                    , ea                            // Previous end angle
                    , eat = sat + a                 // End angle
                    , r = true;

            this.g.lineWidth = this.lineWidth;

            this.o.cursor
                    && (sat = eat - 0.3)
                    && (eat = eat + 0.3);

            if (this.o.displayPrevious) {
              ea = this.startAngle + this.angle(this.value);
              this.o.cursor
                      && (sa = ea - 0.3)
                      && (ea = ea + 0.3);
              this.g.beginPath();
              this.g.strokeStyle = this.previousColor;
              this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
              this.g.stroke();
            }

            this.g.beginPath();
            this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
            this.g.stroke();
            
            this.g.lineWidth = 2;
            this.g.beginPath();
            this.g.strokeStyle = this.o.fgColor;
            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
            this.g.stroke();
           

            return false;
          }
        }
      });
      /* END JQUERY KNOB */
  }
 
  function getGabungData(){
    let gabung1 = JSON.parse($("#dataGabung1").text())
    let labels=[]
    for (let i=0 ; i<gabung1.length;i++){
        console.log(gabung1[i].alias) 
    }
  }
  
  function getDataSet(x){
        
        $.ajax({
        url: `../get_log_now?code=0001&alias=${x}`,
        type: 'GET',
        success: function(res) {
           // console.log(res);
           let val=[]
            result=JSON.parse(res)
            result.forEach((res2)=>{
              //console.log(res2.val)
              val.push(parseFloat(res2.val))
            })
            localStorage.setItem(`gabung2_${x}`, val);
        }
    });
  }
  function getDateSet(x){
      
        $.ajax({
        url: `../get_log_now?code=0001&alias=${x}`,
        type: 'GET',
        success: function(res) {
          let val=[]
            result=JSON.parse(res)
            result.forEach((res2)=>{
              //console.log(res2.tangga
             tg= res2.tanggal.split(" ")
             tg2=tg[1].split(".")
              val.push(res2.tanggal)
            })
            localStorage.setItem(`gabung_${x}`, val);
            
        }
    });
    
  }
  let chart1=document.addEventListener("DOMContentLoaded", () => {
    // let gabung1 = JSON.parse($("#dataGabung1").text())
    // // getDateSet(gabung1[0].alias)
    // // getDataSet(gabung1[0].alias)
    // // getDataSet(gabung1[1].alias)
    // // getDataSet(gabung1[2].alias)
    // // getDataSet(gabung1[3].alias)
    // let dataSet0,dataSet1,dataSet2,dataSet3,dataSet4=[]
    
    // dt=localStorage.getItem(`gabung_${gabung1[0].alias}`);
    // dataSet1=localStorage.getItem(`gabung_${gabung1[0].alias}`);
    // dataSet2=localStorage.getItem(`gabung2_${gabung1[1].alias}`);
    // dataSet3=localStorage.getItem(`gabung2_${gabung1[2].alias}`);
    // dataSet4=localStorage.getItem(`gabung2_${gabung1[3].alias}`);
    lb=[]
    for (let i=1;i<30;i++){
      lb.push((""+i))
    }
    let gabungChart= new Chart(document.querySelector('#recapDevice'), {
      type: 'line',
      data: {
        labels: lb,
      },
      datasets: [
        {
                    label: 'Phase A',
                    data:[0,0,0,0,0,0,0,0], 
                    fill: false,           
                    tension: 1,
                    borderColor: '#00c0ef',
                    backgroundColor: '#00c0ef57', 
                  },
                  {
                    label: 'Phase B',
                    data:[0,0,0,0,0,0,0,0],
                    fill: true,
                    tension: 1,
                    borderColor: '#dd4b39',
                    backgroundColor: '#dd4b3957', 
                  }
                  ,{
                    label: 'Phase C',
                    fill: true,
                    data:[0,0,0,0,0,0,0,0],
                    tension: 1,
                    borderColor: '#006400',
                    backgroundColor: '#006400', 
                  },
                  {
                    label: 'Phase N',
                    fill: true,
                    data:[0,0,0,0,0,0,0,0],
                    tension: 1,
                    borderColor: '#f39c12',
                    backgroundColor: '#f39c12', 
                  }
        ]
      ,
      options: {
        
        responsive: true,
        pointDot: true,
        animation: {
              duration: 0
          },
        elements: {
            point:{
                radius: 4
            }
        },
        scales: {
          y:{
            type: 'logarithmic'
          },
          x: {
            ticks: {
                autoSkip: true,
                maxRotation: 0,
                minRotation: 0,
                color: 'black'
            }
          },
        },
        plugins: {
            legend: {
                display: true,
                position: 'rigt',
                align: 'end',
                
                labels: {
                    color: 'black',
                    font: {
                    weight: 'bold'
                    },
                }
            }
        }
          
      }

    })
    var gabung1 = JSON.parse($("#dataGabung1").text())
    var machine_code=$('#machine_code').text()
    var id_device=$('#id_device').text()
    var array1,array2,array3,array4=[]
    var arrayGabung =[]
    buf =[]
    try{
    for(let i=0;i<gabung1.length;i++){
        arrayGabung.push(JSON.parse(localStorage.getItem(`arr_${machine_code}_${id_device}_${gabung1[i].alias}`)))
        buf.push(0)
      }
    }
    catch(err){
       // var array1,array2,array3,array4=[]
        for(let i=0;i<4;i++){
          arrayGabung.push(0)
      }
    }
    //console.log(array1)
    setInterval(()=>{
      console.log(gabung1.length)
      let bdcolor=[ '#00c0ef','#dd4b39','#f39c12','#006400']
      let bgcolor=[ '#00c0ef57','#dd4b3957','#f39c1257','#00640057']
      for (let i =0;i<4;i++){
        let arr=[]
        for (let i=0;i<30;i++){
          arr.push(0)
        }
        gabungChart.data.datasets[i]={data:arr, 
                        fill: true,           
                        tension: 0.8,
                        borderColor:bdcolor[i],
                        backgroundColor: bgcolor[i] }
      }
      for(let i=0;i<gabung1.length;i++){
            buf[i]= JSON.parse(localStorage.getItem(`${machine_code}_${id_device}_${gabung1[i].alias}`))
          
            arrayGabung[i].push(buf[i].val ||0)
            if(arrayGabung[i].length>30){
              arrayGabung[i].shift()
          }

            localStorage.setItem(`arr_${machine_code}_${id_device}_${gabung1[i].alias}`, JSON.stringify( arrayGabung[i]))   
            gabungChart.data.datasets[i]={data:arrayGabung[i], 
                        fill: true,           
                        tension: 0.8,
                        borderColor:bdcolor[i],
                        backgroundColor: bgcolor[i], }
          
        } 
     
        gabungChart.update()
        //console.log(arrayGabung)
        
        
        }, 1000);
      });
//////////////////////////////////////////////////////////chart baru/////////////////////////////////////////////////////////
lb2=[]
    for (let i=1;i<20;i++){
      lb2.push((""+i))
    }
let bdcolor=[ 'red','yellow','blue','#006400']
let bgcolor=['#FF000030','#FFFF0030','#0000FF50','#00640057']
let dataChart = [JSON.parse($('#graph_current').text()),JSON.parse($('#graph_volt').text()),JSON.parse($('#graph_freq').text())]
let current = JSON.parse($('#graph_current').text())
let chart_array =[]
    mins=[50,0,40]
    maxs=[100,100,60]
    for (let i =0 ;i<3;i++){
      
      chart_array.push(new Chart(document.querySelector('#lineChart'+i), {
      type: 'line',
      data: {
        labels: lb2,
        
      },
      options: {
        responsive: true,
        pointDot: true,
        animation: {
              duration: 0
          },
        elements: {
            point:{
                radius: 3
            }
        },
        scales: {
          y:{
            max: maxs[i],
            min: mins[i],
            
            ticks: {
                stepSize: 2
            }
          },
          x: {
            ticks: {
                autoSkip: true,
                maxRotation: 0,
                minRotation: 0
               
            }
          },
        },
        plugins: {
            legend: {
                display: true,
               
                text: ["Apples","jambu"]
            }
        }
          
      }

    })
    )
  }
  //////////////////////////chart interval ///////////////////////////////

  currentFuse=[[[],[],[]],[[],[],[]],[[]]]
  currentBuf=[]
  // setInterval(()=>{
  //   for (let h =0 ;h<dataChart.length;h++){    
  //     for (let i =0 ;i<dataChart[h].length;i++){
  //       currentData= JSON.parse(localStorage.getItem(`${dataChart[h][i].machine_code}_${dataChart[h][i].id_device}_${dataChart[h][i].alias}`))
  //       currentFuse[h][i].push(parseFloat(currentData.val))
  //       if(currentFuse[h][i].length>20){
  //         currentFuse[h][i].shift()
  //       }
  //       chart_array[h].data.datasets[i]={data:currentFuse[h][i], 
  //                           fill: true,           
  //                           tension: 0.8,
  //                           label:dataChart[h][i].name,
  //                           borderColor:bdcolor[i],
  //                           backgroundColor:bgcolor[i]
  //                        }
  //       }
  //       chart_array[h].update()
  //   }
  // },500)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


 
</script>
</body>
</html>
