<!-------------------------------------------------------*/
/* Copyright   : Amin Rusydi                             */
/* Hayooooooooooo Ngintip ajah nih.......               */
/*-------------------------------------------------------->
<?php
ini_set('display_errors', '0');
ini_set('error_reporting', E_ALL);

?>

<!-- Content Header (Page header) -->

<section class="content-header">

<!-- <script src="https://code.jquery.com/jquery-3.6.0.js"></script> -->

<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<!-- <script src="<?php echo base_url('assets/bower_components/grafik/jquery-3.4.0.min.js')?>"></script> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/paho-mqtt/1.0.2/mqttws31.min.js"></script>
<script src="<?php echo base_url('assets/js/data-mqtt.js') ?>"></script>

<script src="<?php echo base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
<style>
.leaflet-popup-content-wrapper, .leaflet-popup-tip {
  background-color: #063970
}
.bg-point{
  background-color: #063970;
  color:white;
}

</style>
<link rel="stylesheet" href="<?php echo base_url('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')?>">


  <script type="text/javascript">
   // startConnect();
   setInterval("cuacaUpdate();", 2000);

    function cuacaUpdate() {
     // $('#drag_6').load(location.href + ' #updateTable');
      updateMap()
    }
    //tabel dirubah
  </script>

  <script type="text/javascript">
    setInterval("meteringUpdate();", 2000);

    function meteringUpdate() {
      console.log(location.href + ' #updateMetering')
      $('#refreshMetering').load(location.href + ' #updateMetering');
    }
  </script>

  <script type="text/javascript">
    var eventFlag=0;
    var distFlag =0;
    setInterval("scriptUpdate();", 2000);

    function scriptUpdate() {
      //$('#refresh').load(location.href + ' #update');
      //$('#drag6').load(location.href + ' #update');
      
      // $('#refreshh').load(location.href + ' #updatee');
      // $('#refreshGambar').load(location.href + ' #updateGambar');
      $("#responsecontainer").load("<?php echo base_url() . 'grafik'; ?>");
      $("#pie").load("<?php echo base_url() . 'pie'; ?>");
      read_totalEvent()
      
      read_totalDiturbance()
    }

    function read_totalEvent(){
      $.get('event/get_count_event',(res)=>{
        let dta = JSON.parse(res)
       $('#tot_event').text(dta[0].count_even+" Event")
       
       eventData=parseInt(dta[0].count_even)
        if(eventData!=eventFlag){
         // alert("New Event")
        $('#filter_event').val("")
        filterEvent()
          eventFlag=eventData
        }
      })
    }
    function read_totalDiturbance(){
      $.get('data/get_count_data',(res)=>{
        let dta = JSON.parse(res)
        console.log(dta[0].count)
       
       $('#tot_disturbance').text(dta[0].count+" Disturbance")
       distData=parseInt(dta[0].count)
        if(distData!=distFlag){
         
        $('#filter_device').val("")
        filterdata()
          distFlag=distData
        }
      })
      
    }

    
    function updateMap(){
      console.log()
      $.get('beranda/map_condition',(res)=>{
       
        //console.log(marker)
        let dt=JSON.parse(res)
        for (let i = 0 ; i< dt.length;i++){
        let jml=parseInt(dt[i].difference)
          if(jml>5){
          marker[i].setIcon(grayIcon)
         
        }
          else{
            marker[i].setIcon(greenIcon)
        }
      }
      })
    }
  </script>



  <h3 class="d-inline" style="margin-top:5px; font-size:18.5px">
    Device Monitoring System
    <small>Bring Live Your Device</small>
  </h3>
   
  <button class="btn bg-navy" onClick="checkbox()" ><span class="fas fa-lock pic-lock"> </span></button>
  <button class="btn btn-danger  hide btn-param" onClick="showConfig()"><span class="fas fa-plus "> Add Device</span></button>
 <!-- <button  class="btn btn-success  hide btn-param" onClick="showGroup()"><span class="fas fa-plus "> Add Group</span></button>
  -->
  <input hidden type="checkbox" id="lockcheck" >
  <ol class="breadcrumb">
    <li><a href="admin"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
  
</section>

<!-- Main content -->
<section class="content col-md-12 bg-black" style="position:relative;overflow:auto; height: 200vh;">

  <!-- Default box -->
  <?php if ($login == 'T') { ?>

    <div class="row">
      <div class="col-sm-8">
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-info-circle"></i> Dear <?php echo $username ?></h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <ul>
              <li> Sepertinya Anda belum mengganti password pada menu <b>user</b>, demi keamanan maka perbarui password Anda. </li>
              <li> Jangan lupa lengkapi data pribadi Anda pada menu <b>Data Mahasiswa.</b> </li>
              <ul>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->

    </div>
    <!-- /.row --> <?php } ?>




    <?php
  $drag1=$gui_static[0];
  $drag2=$gui_static[1];
  $drag3=$gui_static[2];
  $drag4=$gui_static[3];
  $drag5=$gui_static[4];
  $drag6=$gui_static[5];
  $drag7=$gui_static[6];
  
  //$drag8=$gui[7];

  //var_dump($drag1);
  
  ?>
  <!-- =========================================================================================================== -->
  <!-- Info boxes -->
  <!-- <div class="box-header with-border" style="background-color:#ffffff; border-radius:10px; margin-bottom: 10px;">
			  <h3 class="box-title"><i class="fa fa-area-chart"></i>  Monitoring Panel</h3>
			</div> -->



  <div class="clearfix visible-sm-block"></div>
  <!-- TABLE: Total Disturbance -->
  
<!-- ===================================================Dynamic Data============================================================ -->

<?php foreach ($gui_dyn as $data) {?>
<div id="<?= $data->id_gui?>" class="drag drag-lock" style="position:absolute;min-width:200px;z-index:5;top:<?=$data->y_axes;?>px; left:<?=$data->x_axes;?>px">
      <a style="color:black;" href="#">
        <div class="info-box  bg-navy sty_<?=$data->machine_code;?>_<?=$data->id_device?>_<?=$data->alias?>" >
        <i class="fas fa-close close-lock hide text-blue" style="float:right;margin:5px" onclick="del_gui('<?php echo $data->id_gui?>')"></i>
            <span class="info-box-text text-center drag-point"><?php echo $data->rack_location;?></span>
            <span class="info-box-text text-center drag-point"><?php echo $data->name ;?></span>
            <p  class="info-box-number hide text-center s_high_<?=$data->machine_code;?>_<?=$data->id_device?>_<?=$data->alias?>"><?=$data->status_high;?></p>
            <p class="info-box-number hide text-center s_low_<?=$data->machine_code;?>_<?=$data->id_device?>_<?=$data->alias?>"><?=$data->status_low;?></p>
            
            <p class="info-box-number hide text-center dvdr_<?=$data->machine_code;?>_<?=$data->id_device?>_<?=$data->alias?>"><?=$data->divider;?></p>
            <span class="info-box-number text-center dtg_<?=$data->machine_code;?>_<?=$data->id_device?>_<?=$data->alias?>" style="font-size:30px ;height:100%;line-height:0px;margin-top:20px">0</span>

          
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div></a>
    <!-- /.col -->
<?php }?>





    <div id="drag_1" class=" drag " style="width:400px;position:absolute;z-index:1;top:<?=$drag1->y_axes;?>px; left:<?=$drag1->x_axes;?>px">
      <a style="color:black;" href="<?php  ?>#">
        <div class="info-box bg-navy">
          <span class="info-box-icon bg-orange"><i class="ion ion-ios-pulse"></i></span>
          <div class="info-box-content">
            <span class="info-box-text drag-point key">Total ALL Disturbance</span>
            <span class="info-box-number" id="tot_disturbance"> <?php echo $disturbanceDay . " Disturbance"; ?></span>

          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div></a>
    <!-- /.col -->



    <div id="drag_2" class="drag "style="width:400px;position:absolute;z-index:1;top:<?=$drag2->y_axes;?>px; left:<?=$drag2->x_axes;?>px">
      <a style="color:black;" href=  "#<?php //echo site_url('notification') ?>">
        <div class="info-box bg-navy">
          <span class="info-box-icon bg-yellow"><i class="fa fa-exclamation-triangle"></i></span>

          <div class="info-box-content ">
            <span class="info-box-text drag-point key">Total ALL Event </span>
            <span class="info-box-number" id="tot_event"><?php echo $eventDay . " Event"; ?></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div></a>
    <!-- /.col -->

    <div id="drag_3" class="drag"style="width:400px;position:absolute;z-index:1;top:<?=$drag3->y_axes;?>px; left:<?=$drag3->x_axes;?>px"; >
      <a style="color:black;" href="#">
        <div class="info-box bg-navy">
          <span class="info-box-icon bg-green"><i class="fa fa-code-fork"></i></span>

          <div class="info-box-content">
            <span class="info-box-text drag-point key" >Total Device</span>
            <span class="info-box-number"><?php echo sizeof($all_device); ?> Relay Registered</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div></a>
    <!-- /.col -->


  <div id="drag_4" class="row resize-lock bg-navy " style="width:<?=$drag4->width;?>px;padding:0px;position:absolute;z-index:0;top:<?=$drag4->y_axes;?>px; left:<?=$drag4->x_axes;?>px;">
    <!-- /.col (LEFT) -->
    <!-- <div class=" col-md-12"> -->
      <!-- LINE CHART -->
      <div class="box box-solid bg-navy " style="margin:0px">
        <div class="box-header drag-point key">
          <h3 class="box-title ">Coverage Area</h3>
        </div>
        <div class="box-body" >
          <?php
          $kodePemetaan = array();
          $kodeHasil =  array();
          foreach ($pemetaan as $value) {
            array_push($kodePemetaan, $value->machine_code);
          }

          foreach ($countDisturbance as $dataajahh) {
            array_push($kodeHasil, $dataajahh->machine_code);
          }
          $result = array_diff($kodePemetaan, $kodeHasil);
          ?>
          <div id="map" style="min-height:330px;height:90%;z-index:4;" ></div>

        
        </div>
        
    
        <!-- /.box-body -->
      <!-- </div>
      /.box -->

    </div>
   

    <section hidden class="col-lg-6"style ="  display:height:300px;flex;flex-direction: row;">
       
      <!-- ////////////////////// monitor cuy/////////////// -->
      <?php  
      foreach ($monitor  as $dt) {
        $type ="xx";
        if($dt->port_type=="0"){
          $type="Modbus";
        }
        else if($dt->port_type=="1"){
          $type="TCP-IP";
        }
        if($dt->port_type=="2"){
          $type="IEC61850";
        }
        ?>
      <div class="col-md-6">
        <div class="info-box" style="border-radius: 10px;">
           <span class="info-box-icon" style="background-color: white; border-radius: 10px; padding-top:20px; padding-left:10px;">
            <!-- <div id="perMDCA" class="gauge" style="width: 70px; --rotation:0deg; --color:#5cb85c; --background:#e9ecef;"> -->
            <!-- ${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias} -->
        <?php 
          if($dt->data_type=="boolean"){
            echo '<div id="dt_'.$dt->machine_code.'_'.$dt->id_device.'_'.$dt->alias.'" class="lampu " style="width: 50px;height: 50px;">';
          }
          else if($dt->data_type=="float"||$dt->data_type=="integer"||$dt->data_type=="persen"){
            echo  '<div id="dt_'.$dt->machine_code.'_'.$dt->id_device.'_'.$dt->alias.'" class="gauge" style="width: 70px; --rotation:0deg; --color:#5cb85c; --background:#e9ecef;">';
          }
          else if($dt->data_type=="utc-time"){
            echo '<div id="dt_'.$dt->machine_code.'_'.$dt->id_device.'_'.$dt->alias.'" class="	text-primary" style="width: 50px;height: 50px;margin-top:-20px;margin-left:10px;"><i class="far fa-clock " ></i>';
          }
          else if($dt->data_type=="visible-string"){
            echo '<div id="dt_'.$dt->machine_code.'_'.$dt->id_device.'_'.$dt->alias.'" class="	text-success" style="width: 50px;height: 50px;margin-top:-20px;margin-left:10px;"><i class="fas fa-info " ></i>';
          }
          ?>          
            <div class="percentage"></div>
              <div class="mask"></div>
              <span class="value"></span>
            </div>
          </span>
          <div class="info-box-content">
            <span class="info-box-text"><?php echo $dt->name; ?></span>
            <span class="label label-success"><?php echo $dt->type; ?></span> <span class="label label-success"><?php echo $type;?> </span>
            <span class="info-box-number" id="<?php echo 'val_'.$dt->machine_code.'_'.$dt->id_device.'_'.$dt->alias  ?>">0</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
    <?php }?> 
    <!-- --------------------------------------------------------------------     -->

    </section>
    <!-- /.col -->
    <!-- </div> -->

  </div>



  <!-- <section hidden  class="col-md-12" style =" height:410px;flex;flex-direction: row;background-color: white;">
    <img src="assets/img/lan.png" style="width:400px;height:170px; display: block;margin-left: auto;margin-right: auto;" alt="Flowers in Chania">
      <p class="label-success" style="margin-left: auto;margin-right: auto;margin-top:-20px;"><b><center >DMS 0001 GI WARU</center></b></p>
    <div style="margin-left:50px">
      <table>
      <tr>
        <th style="width:250px;">TCP/IP</th>
        <th style="width:250px;">RS485</th>
      </tr>
      <tr>
        <td style="height:30px;" ><i class="fas fa-network-wired text-success fa-2xl" style="font-size:20px;"></i> &nbsp IP:192.168.0.1 - SEL849 </td>
        <td><i class="fas fas fa-charging-station text-success fa-2xl" style="font-size:20px;"></i> &nbsp PORT:1 SEL849</td>
      </tr>
      <tr>
        <td style="height:30px;" ><i class="fas fa-network-wired text-white fa-2xl" style="font-size:20px;"></i> &nbsp IP:192.168.0.1 - SEL849</td>
      </tr>
      </table>
    </div>
    </section> -->


  <p id="monitorData" hidden ><?php echo JSON_encode( $monitor);?></p>
  <p id="deviceData" hidden><?php echo JSON_encode( $device);?></p>
  
 <!-- /////////////////////////////// rubah group///////////////////////////////////// -->

 <?php foreach ($gui_group as $data) {?>
 <fieldset id="<?= $data->id_gui?>" class= "  resize drag drag-point" style ="border:3px solid white; width:<?=$data->width ?>px ;position:absolute;padding:5px;z-index:5;top:<?=$data->y_axes;?>px; left:<?=$data->x_axes;?>px">
 <!-- <i class="fas fa-close close-lock hide text-blue" style="float:right;margin:5px" onclick="del_gui('<?php  $data->id_gui ;?>')"></i> -->
<legend class="float-left group-<?= $data->id_gui?>" style="border: 0;width:400px;color:white; width: max-content; "><?php echo $data->name;?>&nbsp;
<button style="width:30px;border:0;background-color:transparent" onclick="addGroup('<?= $data->id_gui?>')">+&nbsp;</button>&nbsp;
</legend>

  <div  style="display:flex;flex-wrap:wrap;margin-top:-20px;">
<?php
foreach ($gui_child as $child ) {
  if($child->id_gui==$data->id_gui){
  ?> 
  <div class=" bg-navy sty_<?=$child->machine_code;?>_<?=$child->id_device?>_<?=$child->alias?>"  style="margin:1px;width:150px;height:60px">
      <div class="text-center ">
        <span class=""><?php echo $child->name ;?></span>
          <b><span class="info-box-number dtg_<?=$child->machine_code;?>_<?=$child->id_device?>_<?=$child->alias?>" style="font-size:30px ;line-height:0px;margin-top:20px" >0</span></b>
      
    </div>
  </div>
<?php
 }} ?>


  </div>
</fieldset>
<?php }?>
<!-- ////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////////////////////////////////////////////////////////// -->


      <div hidden id="" class= "col-lg-12 drag ">
        <div id= "mon-body" class="" style="">
          
          <?php $i = 0; 
            foreach(array_reverse($all_device)  as $data)
            { ?>
                  
            <div style="border-bottom:2px solid #00c0ef;padding-bottom:10px;" class="drag-point key">
              <h5 style=" flex-wrap:wrap; color:white;border-bottom:1px solid #00c0ef;">
                <div id="lh_<?= ($device[$i]->id_device);?>" class='light-handler' style="display:inline-block;"></div>
                <b style="font-size: 1.8em;margin: 0em 0.5em;"><?= ($device[$i]->rack_location)?>- <?=($device[$i]->type);?></b>
              </h5> 
              <div style="display:flex;flex-wrap:wrap ; gap: 0.3em ;margin-left:5px; " id="bd_<?= ($device[$i]->id_device); $i++;?>">   
              </div>
            </div>
              
      <?php }?>
            
            <div hidden class="text-center"><a href="device/index/0001"><h5 class="text-white"style="color:white">More Details</h5></a></div> 
          </div>
      </div>

      <div class="col-lg-12 drag resize" id="drag_5"style="position:absolute;top:<?=$drag5->y_axes;?>px; left:<?=$drag5->x_axes;?>px">
        <div class="box box-solid bg-navy">
          <div class="box-header  drag-point key">
            <h3 class="box-title "><b>Device List</b></h3>
          </div>
          
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin" id="tb_device">
            <thead>
              <tr>
                <th><b>GI NAME</b></th>
                <th><b>MACHINE CODE</b></th>
                <th><b>RELAY TYPE</b></th> 
                <th><b>RACK LOCATION</b></th>
                <th><b>PROTOCOL</b></th>
                <th><b>MORE DETAIL</b></th>
              </tr>
            </thead>
            <tbody>
              <?php   foreach($device  as $data){?>
              <?php
                $port="";
                if ($data->port_type==0){
                  $port = "Modbus ID ".$data->id_device;
                }
                else if($data->port_type==1){
                  $port = "TCP/IP IP : ".$data->ip_address; 
                }
                else if($data->port_type==2){
                  $port = "IEC61850";
                }?>
                
                <tr>
                  <td><?=$data->giname?></td>
                  <td><?=$data->machine_code?></td>  
                  <td><?=$data->type?></td>
                  <td><?=$data->rack_location;?></td>
                  <td><?=$port;?></td>
                  <td><a href="device_relay/index/<?=$data->no ?>" class="small-box-footer" >More Detail <i class="fa fa-arrow-circle-right" > </i></a></td>
                </tr>
              <?php } ?>
            </tbody>
            </table>
            
            </div>
            
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <!-- box-footer div dibawah ini dipakai ? -->
          <!-- <div class="box-footer clearfix">
          </div> -->
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    
<!-- /////////////////////////////////////////////////////////////////////////////////////////////////// -->


  <div  class="row hidden ">
    <!-- /.col (LEFT) -->
    <div class="col-md-6 " >
      <!-- LINE CHART -->
      <div class="box box-solid bg-navy">
        <div class="box-header">
          <h3 class="box-title">Disturbance Record per Month</h3>

          <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div> 
        </div>
        <div class="box-body">
          <div class="chart">
            <!-- <canvas id="lineChart" style="height:250px"></canvas> -->
            <div id="responsecontainer"></div>

          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>
<!-- 
//////////////////////////////////////////////////////////////////////////////// -->

    <div id="drag_6"  class="drag resize resize-lock" style=" position:absolute;width:<?=$drag6->width;?>px ;top:<?=$drag6->y_axes;?>px; left:<?=$drag6->x_axes;?>px" >
      <div id="updateTable" class="">
        <div class="box box-solid bg-navy" style="min-height:400px;">
          <div class="box-header drag-point key">
            <h3 class="box-title">Latest Disturbance Records</h3>
            <div class="input-group col-md-5" style="float:right;margin-right: 1em;">
          <input type="text" id="filter_device" class="form-control bg-navy" placeholder="Search device">
            <span class="input-group-btn">
              <button type="submit" name="search" onclick="filterdata()" id="search-btn" class="btn btn-primary"><i class="fa fa-search"></i>
              </button>
            </span>
          </div>
          </div>
          
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin" id="tb_dist">
                <thead>
                  <tr>
                    <th>Device</th>
                    <th>Relay Type</th>
                    <th>Description</th>
                    <!-- <th>Status</th> -->
                    <th>GI Name</th>
                    <th>Time</th>
                   
                  </tr>
                </thead>
                <tbody id="tb-log">
                 
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix bg-navy">
            <a href="data" class="btn btn-sm btn-warning btn-flat pull-right">View All Records</a>
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>

    <div id="drag_7"  class="drag resize resize-lock" style="position:absolute; width:<?=$drag7->width;?>px ;top:<?=$drag7->y_axes;?>px; left:<?=$drag7->x_axes;?>px" >
      <div id="updateTable2" class="">
        <div class="box box-solid bg-navy" style="min-height:400px;">
          <div class="box-header drag-point key">
            <h3 class="box-title">Latest Event Records</h3>
            <div class="input-group col-md-5" style="float:right;margin-right: 1em;">
          <input type="text" id="filter_event" class="form-control bg-navy" placeholder="Search device"> 
            <span class="input-group-btn">
              <button type="submit" name="search" onclick="filterEvent()" id="search-btn" class="btn btn-primary"><i class="fa fa-search"></i>
              </button>
            </span>
          </div>
          </div>
          
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin" id="tb_dist">
                <thead>
                  <tr>
                    <th>Device</th>
                    <th>Relay Type</th>
                    <th>Event</th>
                    <th>GI Name</th>
                    <th>Time</th>
                   
                  </tr>
                </thead>
                <tbody id="tb-event">
                  <?php $i = 0;
                  
                  foreach ($event_data  as $data) { ?>
                    <tr>
                      <td><?=$data->machine_code; ?></td>
                      <td><?=$data->type; ?></td>
                     
                      <?php $dataStatus = ucwords($data->status);
                      if ($dataStatus == "Healthy" ||$dataStatus == "Normal"  ||$dataStatus == "Normal") { ?>
                        <td><span class="label label-success">Healthy</span></td>
                      <?php } else { ?>
                        <td><span class="label label-danger"><?php echo $dataStatus;?></span></td>
                      <?php } ?>
                      <td><?=$data->lokasi; ?></td>
                      <td>
                        <?=$data->tanggal . " " . $data->waktu . " WIB"; ?>
                        <!-- <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div> -->
                      </td>
                     
                    </tr>

                  <?php if (++$i >= 6) break;
                  } ?>
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix bg-navy">
            <a href="event" class="btn btn-sm btn-warning btn-flat pull-right">View All Records</a>
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>



  </div>


  
  <!-- /.row -->
  <div hidden class="box-footer bg-navy" style="text-align:center">
    DMS &copy<a href=""><strong> PT. Micronet Gigatech Indoglobal</strong></a> 2023
  </div>

  <!-- =========================================================================================================== -->



  <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer hidden class="main-footer bg-navy">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <br>
    <!-- <strong></strong> -->
  </footer>

  <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->
<!-- =============================================Modal============================================================================== -->
<!-- Modal -->
<div class="modal  modal-vertical-centered fade" id="configModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content bg-navy">
      <div class="modal-header">
       
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
       
          <span class="text-white" aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="height:200px">
      </div>

    </div>
  </div>
</div>

<!-- =============================================end Modal ========================================================================= -->
  <!-- jQuery 3 -->
  
  <!-- <script src="<?php echo base_url('assets/bower_components/grafik/jquery-3.4.0.min.js') ?>"></script> -->
  <!-- <script src="<?php //echo base_url('assets/bower_components/jquery/dist/jquery.min.js') ?>"></script> -->
<!-- jquery-ui -->

<!-- <script src="<?php //echo base_url('assets/bower_components/jquery/dist/jquery-ui.js')?>"></script> -->

  <!-- Bootstrap 3.3.7 -->
  <script src="<?php echo base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
  <!-- SlimScroll -->
  <script src="<?php echo base_url('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
  <!-- FastClick -->
  <script src="<?php echo base_url('assets/bower_components/fastclick/lib/fastclick.js') ?>"></script>


  <script src="assets/bower_components/select2/dist/js/select2.full.min.js"></script>
  <!-- InputMask -->
  <script src="assets/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>



  <script src="<?php echo base_url('assets/bower_components/chart.js/Chart.js') ?>"></script>
  <script src="<?php echo base_url('assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>

  <script src="<?php echo base_url('assets/bower_components/grafik/mdb.min.js') ?>"></script>
  

  


  <!-- AdminLTE for demo purposes -->
  <!-- AdminLTE App -->
  <script src="<?php echo base_url('assets/js/adminlte.min.js') ?>"></script>
  
  <script src="<?php echo base_url('assets/bower_components/Flot/jquery.flot.js') ?>"></script>
  <script src="<?php echo base_url('assets/bower_components/Flot/jquery.flot.categories.js') ?>"></script>




  <script>
    var HubIcon = L.Icon.extend({
      options: {
        iconSize: [65, 65],
        iconAnchor: [22, 39],
        shadowAnchor: [4, 62],
        popupAnchor: [-3, -40]
      }
    });
    var greenIcon = new HubIcon({
      iconUrl: 'assets/img/switch1.png'
    });
    var grayIcon = new HubIcon({
      iconUrl: 'assets/img/switch3.png'
    });

    var redIcon = new HubIcon({
      iconUrl: 'assets/img/switch2.png'
    });

    var map = L.map('map').setView([ -7.797068, 110.370529], 6);

    var customOptions = {
      'maxWidth': '60px',
      'className': 'custom'
    };


    // https://tile.openstreetmap.org/{z}/{x}/{y}.png
    let marker=[]
    L.tileLayer('https://tile.jawg.io/jawg-dark/{z}/{x}/{y}.png?access-token=cRC4wKMYnFlJ9D9JFobqKUFgIOpLiWSpAo8Ydjn524LpMUD29WNyjR6s5EJjL1Gf', {
      maxZoom: 100,
      id: 'mapLocation',
    }).addTo(map);

    <?php date_default_timezone_set('Asia/Jakarta');
    $tanggal = date('Y-m-d');

    foreach ($pemetaan as $value) {
      foreach ($result as $a => $hasil) {
        if ($value->machine_code == $hasil) {
          $find = 0;
          $find2=0;
        }
      }
      foreach ($countDisturbance as $codeDisturbance) {
        if ($codeDisturbance->machine_code == $value->machine_code) {
          $find = $codeDisturbance->jumlah;
        }
      }
      foreach ($countEvent as $codeEvent) {
        if ($codeEvent->machine_code == $value->machine_code) {
          $find2 = $codeEvent->jumlah;
          //echo $find2;
        }
      }
      if ($find > 0) {
        $image = "redIcon";
      } else {
        $image = "greenIcon";
      } ?>

      var customPopup = `<div style="width:250px;"class="bg-point">
                          <div class='card-body bg-point'>
                            <h5 class='card-title'><?= $value->device_name ?> </h5>
                              <ul class='list-group list-group-flush bg-point'>
                                <li class='bg-point list-group-item'>Machine Code
                                  <span class='badge bg-primary rounded-pill'><?=  $value->machine_code?></span>
                                </li>
                                <li class='list-group-item bg-point'>Location
                                  <span class='badge bg-primary rounded-pill'> <?= $value->location  ?> </span>
                                </li>
                                <li class='list-group-item bg-point'>GI Name
                                  <span class='badge bg-primary rounded-pill'> <?= $value->gi_name  ?> </span>
                                </li>
                               
                                <li bg-point class='list-group-item bg-point'>Total ALL Disturbance : 
                                  <span class='badge bg-primary rounded-pill'> <?=  $find  ?></span>
                                </li>
                                
                              </ul><a style='color:white;' href='device/index/<?=  $value->machine_code ?>' class='btn btn-primary'>Detail</a></div></div>`;
     
      marker.push(L.marker([<?= $value->latitude ?>, <?= $value->longitude ?>], {
          icon: <?= $image ?>,
          radius: 90000,
          color: 'red',
          fillColor: 'red',
          fillOpacity: 0.5,
        }).bindPopup(customPopup, customOptions)
        .addTo(map));

    <?php }  ?>
    console.log(marker) 
    function readDevice(){
      $.get("mon/apiReadMonLimit",(res)=>{
       
        // $(`#mon_data`).val(res)

        dt = JSON.parse(res)
        buf= dt.data
        //  appendWidget(buf)
      })}
    function appendWidget(buf){
      //buf = data
      console.log(buf)
      for(let i =0 ;i<buf.length;i++){

        let data_type= ""
        if(buf[i].data_type=="float" || buf[i].data_type=="integer"){
          data_type=`<i class="icon_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias} text-yellow fas fa-bolt fa-lg" style="line-height:40px;margin-left:10px"></i>`
         
        }
        else if(buf[i].data_type=="boolean"){
          data_type=`<div class="icon_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias} fa-lg bg-gray" style="margin-left:3px;margin-top:5px;border:2px solid gray; width:20px;height:20px;border-radius:100%"></div>`
         // $(`#lh_${buf[i].id_device}`).append(data_type);
        
        }
        else if(buf[i].data_type=="visible-string"){
          data_type=`<i class="fas fa-info text-white fa-lg" style="line-height:40px;margin-left:10px"></i>`
        }
        else if(buf[i].data_type=="utc-time"){
          data_type=`<i class="far fa-clock text-white fa-lg" style="line-height:40px;margin-left:5px"></i>`
        }
        //if
       let unit =""
       if(buf[i].data_type=="float"||buf[i].data_type=="integer"){
        unit=buf[i].unit
       }

        dthtml=`<div style="width:21.6% ; color:white;padding:0px;display:flex;border: 1px solid;">                    
                    <div class="bg-navy" style="width:100%">
                      <button type="button" style="float: right;" class="btn btn-default btn-sm"><i class="fa fa-close"></i></button>
                      <h6 style="padding: 2px;font-size: 1.2em;float: right;margin-right: 4px;border: 1px solid;margin-top: 4px;">
                        ${buf[i].name}  
                        
                      </h6>
                      <h5 hide style="font-size:3em;text-align: center;margin:1em 0em;" id="dtx_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}" >
                       0 ${unit}
                      </h5>
                      
                  </div>
                  `
        $(`#bd_${buf[i].id_device}`).append(dthtml)
        console.log(buf[i].id_device)
      }
    }
    

    function gotoDevice(x){
        alert(x)
    }
function checkbox(){

  $('#lockcheck').click();
  localStorage.setItem("lock", $('#lockcheck').is(':checked'))
  readLock()

}
$(document).ready(function() {
      $('.sidebar-menu').tree()
      readDevice()
      startConnect();
      readLock()
      
    
      

      $('.drag-point').on({
        mouseenter:(ui)=>{
          //console.log("enable")
          if($('#lockcheck').is(':checked')==true){
              $(".drag").draggable('enable')
              $('.drag-point').css( 'cursor', 'move' );
          }
          else{
            $(".drag").draggable('disable')
            $('.drag-point').css( 'cursor', 'default' );
          }
          }  
          ,
        mouseleave:()=>{
          $(".drag").draggable('disable')
          $('.drag-point').css( 'cursor', 'default' );
        }
        

      })
       $(".drag").draggable({
        stop: function(event, ui) {
        var pos = ui.position;
        var id = ui.helper.attr("id");
        console.log(id)
        $.get(`beranda/upd_gui?y=${pos.top}&x=${pos.left}&id_gui=${id}`,(res)=>{
          console.log(res)
        })
       }
      });
      
       

    })
$(".resize").resizable(
        {
          handles: 'e',
           stop: function(event, ui) {
            let id = ui.helper.attr("id");
            console.log(id);
            $.get(`beranda/upd_size?w=${ui.size.width}&id_gui=${id}`,(res)=>{
              console.log(res)
            })
            //upd_size
            console.log(ui)
        }}
      );


function readLock(){
  let dtLock=localStorage.getItem('lock')
  console.log(dtLock)
  if(dtLock=="true"){
    $('#lockcheck').prop('checked',true);
    $('.pic-lock').removeClass('fa-lock')
    $('.pic-lock').addClass('fa-unlock')
    $('.btn-param').removeClass('hide')
    $('.close-lock').removeClass('hide')
    $(".resize").resizable('enable')
  //  $('.resize-lock').addClass('resize')
    }
  else{
    $(".resize").resizable('disable').removeClass('ui-state-disabled');
    $('#lockcheck').prop('checked',false);
    $('.pic-lock').removeClass('fa-unlock')
    $('.pic-lock').addClass('fa-lock')
    $('.btn-param').addClass('hide')
    $('.close-lock').addClass('hide')
    $(".resize").resizable('disable').removeClass('ui-state-disabled');
   // $('.resize-lock').removeClass('resize')
  }
  console.log(dtLock)
}

  function showConfig(){
    $('.modal-body').load('beranda/showConfig')
    $('#configModal').modal('show'); 
  } 
function showGroup(){
    $('.modal-body').load('beranda/showGroup')
    $('#configModal').modal('show'); 
  } 


  function del_gui(x){
    $.get('beranda/del_gui?id_gui='+x,(res)=>{
      console.log(res)
    })
    $(`#${x}`).remove();
  }
function filterEvent(){
  let data = $('#filter_event').val()
    let url=`data/filter_by_event?gi=${data}`
    $.get(url,(res)=>{
      console.log(res)
      $('#tb-event').empty()
      let dt= JSON.parse(res)
      var dt_tabel=``;
      for (let i =0;i<dt.length;i++){
        port= ``
        status=``
        if(dt[i].port_type==0){
          port= ` Port Number :${dt[i].description}`
        }  
        else{
          port= ` IP Number :${dt[i].description}`
        }
        if(dt[i].status=="Healthy"||dt[i].status=="Normal"||dt[i].status=="normal"){
          status=`<td><span class="label label-success">${dt[i].status}</span></td>`
        }  
        else{
          status=`<td><span class="label label-danger">${dt[i].status}</span></td>`
        }
        dt_tabel+= `<tr>
                      <td>${dt[i].machine_code}</td>
                      <td>${dt[i].type}</td>
                      ${status}
                      <td>${dt[i].gi_name}</td>
                      <td>
                          ${dt[i].tanggal} ${dt[i].waktu} WIB
                        <!-- <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div> -->
                      </td>
                      
                    </tr>
                  `
      }
      $('#tb-event').append(dt_tabel)
    })
    console.log('filter')
  }

  function filterdata(){
    let data = $('#filter_device').val()
    let url=`data/filter_by_gi?gi=${data}`
    $.get(url,(res)=>{
      console.log(res)
      $('#tb-log').empty()
      let dt= JSON.parse(res)
      var dt_tabel=``;
      for (let i =0;i<dt.length;i++){
        port= ``
        status=``
        if(dt[i].port_type==0){
          port= ` Port Number :${dt[i].description}`
        }  
        else{
          port= ` IP Number :${dt[i].description}`
        }
        if(dt[i].status=="Healthy"||dt[i].status=="Normal"||dt[i].status=="normal"){
          status=`<td><span class="label label-success">${dt[i].status}</span></td>`
        }  
        else{
          status=`<td><span class="label label-danger">${dt[i].status}</span></td>`
        }
        dt_tabel+= `<tr>
                      <td>${dt[i].machine_code}</td>
                      <td>${dt[i].type}</td>
                      <td>ID RELAY: ${dt[i].relay_id}</td>
                  
                      <td>${dt[i].lokasi}</td>
                      <td>
                          ${dt[i].tanggal} ${dt[i].waktu} WIB
                        <!-- <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div> -->
                      </td>
                      
                    </tr>
                  `
      }
      $('#tb-log').append(dt_tabel)
    })
    console.log('filter')
  }
  function addGroup(x){
   $(`.group-${x}`).append(`
   <div class="bg-navy group_add" style="border:2px solid white; border-radius:10px;width:200px;heigh:300px;position:absolute; z-index:999;padding:10px;left:60px;" > 
  

   </div>
   `) 
   $('.group_add').load('')
    console.log(x)
  }
  // $('#tb_device').DataTable();

  ////////////////////////Config Table//////////////////////////////////////////////
  $('#tb_device thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#tb_device thead');
 
    var table = $('#tb_device').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        initComplete: function () {
            var api = this.api();
            // For each column
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    // Set the header cell to contain the input element
                   
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    
                    if(title!="Action"){
                      $(cell).html('<input type="text" style="width:150px;color:black;" placeholder="' + title + '" />');
                    }
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('change', function (e) {
                            // Get the search value
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();
 
                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();
                        })
                        .on('keyup', function (e) {
                            e.stopPropagation();
 
                            $(this).trigger('change');
                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });
    
  </script>

  </body>

  </html>