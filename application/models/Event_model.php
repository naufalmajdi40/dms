<?php


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Krs_model
class Event_model extends CI_Model
{
    // Property yang bersifat public
    public $table = 'data_event';
    // public $order = 'ASC';
    
	// Konstrutor    
    function __construct()
    {
        parent::__construct();
    }
   
   // Menampilkan semua data 
   function get_all()
   {
       return $this->db->get($this->table)->row();
   }
   function get_all_selected(){
        $qry="select *from im_com left join im_config on im_com.id_com = im_config.com";
        return $this->db->query($qry);
   }
  
   public function baca_data($user)
	{
	  // Jika session data username tidak ada maka akan dialihkan kehalaman login
	  if (!isset($this->session->userdata['username'])) {
		redirect(base_url("login"));
	  }

	  	$this->db->select('*');

		$this->db->from('device_user');
	    $this->db->where('device_user.username', $user);
	    $this->db->join('data_event','data_event.machine_code = device_user.machine_code');
        $this->db->join('device_list','data_event.machine_code = device_list.machine_code');
		$data = $this->db->get()->result();
		return $data;
	}
    function count_event(){
        $qry ="select count(*) as count_even from data_event  ";
        return $this->db->query($qry);
    }

}

/* End of file Krs_model.php */
/* Location: ./application/models/Krs_model.php */
/* Please DO NOT modify this information : */
?>