<?php


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Krs_model
class Ultg_model extends CI_Model
{
    // Property yang bersifat public
    public $table = 'im_ultg';
    public $ultg_id = 'ultg_id';
    public $ultg_loc = 'ultg_loc';
    public $ultg_name ='ultg_name';
	// Konstrutor    
    function __construct()
    {
        parent::__construct();
} 

  
   
   
   // Menampilkan semua data 
function get_all()
    {
    	$this->db->select('*');
		$this->db->from($this->table);
		$this->db->order_by('id', 'desc');
		return $this->db->get()->result();
    }

    
	
 

    // Merubah data kedalam database
    function update($ids, $data)
    {
        $this->db->where('ultg_id',$ids);
        $this->db->update($this->table, $data);
         if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
   

    // Menghapus data kedalam database
    function delete($id)
    {
         $this->db->where('ultg_id', $id);
        $this->db->delete('im_ultg');
        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function get_device($row){
        $qry ="SELECT a.*,c.ultg_name FROM device_list a INNER JOIN device_user b ON a.machine_code = b.machine_code LEFT JOIN im_ultg c on c.ultg_id=a.ultg_id where b.username ='0001' GROUP BY a.machine_code" ;
        return $this->db->query($qry);
    }
    function get_device_perrelay(){
        $qry ="SELECT * from device_list_perdevice";
        return $this->db->query($qry);
    }
    function get_device_ultg(){
        $qry ="SELECT * from im_ultg";
        return $this->db->query($qry);
    }

   

   
 
    function load_ultg(){
        $qry ="SELECT * from im_ultg";
        return $this->db->query($qry);
    }
     function load_ultg_id($id){
        $qry ="SELECT * from im_ultg where ultg_id = '".$id."'";
        return $this->db->query($qry);
    }
    function save_ultg($data,$device){
         $this->db->insert('im_ultg', $data);
        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }
 
}
/* End of file Krs_model.php */
/* Location: ./application/models/Krs_model.php */
/* Please DO NOT modify this information : */