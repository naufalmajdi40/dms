<?php


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Krs_model
class Device_model extends CI_Model
{
    // Property yang bersifat public
    public $table = 'device_list';
    public $id = 'machine_code';
    public $user = 'username';
    public $no = 'no';
    public $order = 'DESC';
	// Konstrutor    
    function __construct()
    {
        parent::__construct();
} 

    function json() {       
		// $username    = $this->session->userdata['username'];
		$this->datatables->select("data.no,data.machine_code,data.relay_id,data.lokasi,data.status,data.nama_file,data.waktu");
        $this->datatables->from('data');
        $this->datatables->add_column('action', '<button type="button" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></button>'); 
        return $this->datatables->generate();
    }
   
   
   // Menampilkan semua data 
function get_all()
    {
    	$this->db->select('*');
		$this->db->from($this->table);
		$this->db->order_by('id', 'desc');
		return $this->db->get()->result();
    }

      function get_data($user)
    {
    	

        $this->db->select('*');
		$this->db->from('device_user');
	    $this->db->where('device_user.username', $user);
	    $this->db->join('device_list','device_list.machine_code = device_user.machine_code');
        $this->db->join('data','data.machine_code = device_list.machine_code');
		$data = $this->db->get()->result();
		return $data;
    }

    function get_map($user)
    {

        $this->db->select('*');
		$this->db->from('device_list');
	    $this->db->where('device_user.username', $user);
        $this->db->order_by('device_list.machine_code', 'asc');
	    $this->db->join('device_user','device_user.machine_code = device_list.machine_code');    
		$data = $this->db->get()->result();
		return $data;
        // $qry ="SELECT a.*,b.*,count(c.machine_code) as jumlah_event from device_list a INNER JOIN device_user b on a.machine_code = b.machine_code LEFT JOIN data_event c on a.machine_code = c.machine_code WHERE b.username='0002' GROUP BY a.machine_code ORDER BY a.machine_code asc" ;
        // return $this->db->query($qry);
    }

    // Menampilkan semua data berdasarkan id-nya
    function get_by_id($user)
    {
        $this->db->where($this->user, $user);
        $this->db->order_by('id', "desc");
        return $this->db->get($this->table)->row();
    }
	
    // Menambahkan data kedalam database
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // Merubah data kedalam database
    function update($ids, $data)
    {
        $this->db->where('id',$ids);
        $this->db->update($this->table, $data);
         if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
   

    // Menghapus data kedalam database
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function get_device($row){
        $qry ="SELECT a.*,c.ultg_name FROM device_list a INNER JOIN device_user b ON a.machine_code = b.machine_code LEFT JOIN im_ultg c on c.ultg_id=a.ultg_id where b.username ='0001' GROUP BY a.machine_code" ;
        return $this->db->query($qry);
    }
    function get_device_perrelay(){
        $qry ="SELECT * from device_list_perdevice";
        return $this->db->query($qry);
    }
    function get_device_ultg(){
        $qry ="SELECT * from im_ultg";
        return $this->db->query($qry);
    }

    function save_dms($data,$device){
         $this->db->insert('device_list', $data);
             $this->db->insert('device_user', $device);
        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function del_dms($id){
        $this->db->where('id', $id);
        $this->db->delete('device_list');
        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function load_dms_id($id){
        $qry ="SELECT a.*,b.ultg_name from device_list a left join im_ultg b on a.ultg_id = b.ultg_id where a.id = ".$id;
        return $this->db->query($qry);
    }
    function load_ultg(){
        $qry ="SELECT * from im_ultg";
        return $this->db->query($qry);
    }
     function load_ultg_id($id){
        $qry ="SELECT * from im_ultg where ultg_id = '".$id."'";
        return $this->db->query($qry);
    }
    function save_ultg($data,$device){
         $this->db->insert('im_ultg', $data);
        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }
     function update_dms($ids, $data)
    {
        $this->db->where('ultg_id',$ids);
        $this->db->update("im_ultg", $data);
         if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
/* End of file Krs_model.php */
/* Location: ./application/models/Krs_model.php */
/* Please DO NOT modify this information : */