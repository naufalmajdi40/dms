  <?php

/* Copyright   : Amin Rusydi                             */
/* Hayooooooooooo Ngintip ajah nih.......               */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Admin
class Beranda extends CI_Controller {
	
	// Konstrutor 
	function __construct() {
		parent::__construct();
		$this->load->model('Users_model');
		$this->load->model('Device_model');
		$this->load->model('Data_model');
		$this->load->model('Mon_model');
	    $this->load->model('Device_user');
		$this->load->model('Notifikasi_model');
		$this->load->library('form_validation');
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	}
	
	// Fungsi untuk menampilkan halaman utama admin
	public function index() {
		// Menampilkan data berdasarkan id-nya yaitu username
		$row = $this->Users_model->get_by_id($this->session->userdata['username']);
	    $deviceUse = $this->Device_user->get_by_id($this->session->userdata['username']);
		$data = array(	
			'username' => $row->username,
			'nama'     => $row->nama,
			'kebun'     => $row->kebun,
			'email'    => $row->email,
			'level'    => $row->level,
		
		);
	
		$id_product=$deviceUse->machine_code;
		$user=$row->username;
	    $level=$row->level;

		if($level=='user'){
			$Sensor=array(
 
				'sensor_data'=>$this->baca_data($user),
				'event_data'=>$this->baca_data_event($user),
				'disturbanceDay'=>$this->disturbance_day($user),
				'eventDay'=>$this->event_day($user),
				'disturbanceDevice'=>$this->disturbance_device($id_product),
				'countDisturbance'=>$this->count_disturbance($user),
				'countEvent'=>$this->count_event($user),
				
				'pieData'=>$this->pie_chart($id_product),
				'notif'=>$this->user_notif($user),
				'id_product'=>$id_product,
				'pemetaan'=>$this->Device_model->get_map($user),
				'monitor'=>$this->monitor_data(),
				'device'=>$this->device_list_device(),
				'all_device'=>$this->Mon_model->get_device_permesin_all()->result(),
				'gui'=>$this->read_gui(),
				'gui_static'=>$this->read_gui_static(),
				'gui_dyn'=>$this->read_gui_dyn(),
				'gui_group'=>$this->read_gui_group(),
				'gui_child'=>$this->read_gui_child(),
				'dms_list'=>$this->read_dms(),
				'relay_list'=>$this->read_relay(),
				// 'jenis_tanaman'=>$this->Tanaman_model->get_by_id($id_product)->jenis_tanaman, 
				// 'tgl_panen'=>$this->Tanaman_model->get_by_id($id_product)->tgl_panen,
				// 'latitude'=>$this->Notifikasi_model->get_by_id($id_product)->latitude,
				// 'gambar'=>$this->Notifikasi_model->get_by_id($id_product)->gambar,
				// 'longitude'=>$this->Notifikasi_model->get_by_id($id_product)->longitude,);
				// 'tgl_tanam'=>$this->Tanaman_model->get_by_id($id_product)->tgl_tanam,
			);
	
			$this->load->view('header',$data);
			$this->load->view('beranda',$Sensor); // Menampilkan halaman utama admin
		}
		
		if($level=='admin'){
			$Sensor=array(
				  
				'sensor_data'=>$this->baca_data($user),
				'event_data'=>$this->baca_data_event($user),
				'disturbanceDay'=>$this->disturbance_day($user),
				'disturbanceDevice'=>$this->disturbance_device($id_product),
				'countDisturbance'=>$this->count_disturbance($user),
				'countEvent'=>$this->count_event($user),
				
				'pieData'=>$this->pie_chart($id_product),
				'notif'=>$this->user_notif($user),
				'id_product'=>$id_product,
				'pemetaan'=>$this->Device_model->get_map($user), 
				'monitor'=>$this->monitor_data(),
				'device'=>$this->device_list_device(),
				'all_device'=>$this->Mon_model->get_device_permesin_all()->result(),
				'gui'=>$this->read_gui(),
				'gui_static'=>$this->read_gui_static(),
				'gui_dyn'=>$this->read_gui_dyn(),
				'gui_group'=>$this->read_gui_group(),
				'gui_child'=>$this->read_gui_child(),
				'dms_list'=>$this->read_dms(),
					'relay_list'=>$this->read_relay(),
			
			);
	
			$this->load->view('header',$data);
			$this->load->view('beranda2',$Sensor); // Menampilkan halaman utama admin
		}
		
	}
	public function map_condition(){
		$row = $this->Users_model->get_by_id($this->session->userdata['username']);
		$user=$row->username;
		$qry="SELECT
				a.machine_code,
				TIMESTAMPDIFF(
					MINUTE,
					a.last_update,
				now()) AS difference 
			FROM
				device_list a 
				INNER JOIN device_user b
				on a.machine_code = b.machine_code WHERE b.username='".$this->session->userdata['username']."' order by a.machine_code asc";
		$row= $this->db->query($qry)->result();
		echo json_encode($row);
	}
	public function showConfig(){
		$Sensor=array(
			'device'=>$this-> read_device()
		);
		$this->load->view('config/param_config',$Sensor);
	}
	public function showGroup(){
		$Sensor=array(
			'device'=>$this-> read_device()
		);
		$this->load->view('config/param_group',$Sensor);
	}
	public function addGroup(){
		$Sensor=array(
			'device'=>$this-> read_device()
		);
		$this->load->view('config/add_group',$Sensor);
	}
	public function read_device(){
		$qry ="select * FROM device_list_perdevice";
		return $this->db->query($qry)->result();
	}
    ///api di beranda 
	public function apiReadMonbyId(){
		$machine=$this->input->get('machine');
		$id=$this->input->get('id');
        $row['data']=$this->Mon_model->get_all_byid($machine,$id)->result();
        echo json_encode($row);
    }
	public function apiFilterGroup(){
		$machine_code=$this->input->get('machine_code');
		$id=$this->input->get('id');
		$group =$this->input->get('group');
        $row['data']=$this->Mon_model->filterGroup($machine_code,$id,$group)->result();
        echo json_encode($row);
    }


	public function insertParam(){
		$machine_code =$this->input->get('machine_code');
		$id_device =$this->input->get('id_device');
		$alias = $this->input->get('alias');
		$id_gui = "drag_".time();
		$unit = $this->input->get('unit');
		$name = $this->input->get('name');
		$rack_location= $this->input->get('rack_location');
		$data_type= $this->input->get('data_type');
		$divider= $this->input->get('divider');
		$qry ="insert into gui (id_gui,type,machine_code,id_device,alias,unit,name,rack_location,data_type,divider) values ('$id_gui','dynamic','$machine_code','$id_device','$alias','$unit','$name','$rack_location','$data_type',$divider)";
		//echo $qry;
		$row=$this->db->query($qry);
		if($row){
            echo json_encode(
                array(
                    'success' => true,
                    'message' => 'Update Success',
                
                )
            );
        }
        else {
            echo json_encode(
                array(
                    'success' => false,
                    'message' => 'Update Fail',
                
                )
            );
        };
	}

	//////////////////////////////////////////////////////////
	public function insertGroup(){
		$machine_code =$this->input->get('machine_code');
		$id_device =$this->input->get('id_device');
		$group_name=$this->input->get('group_name');
		$id_gui = "drag_".time();
		// $alias = $this->input->get('alias');
		// $id_gui = "drag_".time();
		// $unit = $this->input->get('unit');
		// $name = $this->input->get('name');
		// $rack_location= $this->input->get('rack_location');
		// $data_type= $this->input->get('data_type');
		// $divider= $this->input->get('divider');
		$qry ="insert into gui (type,width,id_gui,machine_code,id_device,name) values ('group',400,'$id_gui','$machine_code','$id_device','$group_name')";
		//echo $qry;
		$row=$this->db->query($qry);
		if($row){
            echo json_encode(
                array(
                    'success' => true,
                    'message' => 'Update Success',
                
                )
            );
        }
        else {
            echo json_encode(
                array(
                    'success' => false,
                    'message' => 'Update Fail',
                
                )
            );
        };
	}
	/////////////////////////////////////////////////////////
	public function read_gui(){
		$qry ="select * FROM gui ";
		return $this->db->query($qry)->result();
	}
	public function read_dms(){
		$qry ="select * FROM device_list ";
		return $this->db->query($qry)->result();
	}
	public function read_relay(){
		$qry ="select * FROM device_list_perdevice";
		return $this->db->query($qry)->result();
	}

	public function read_gui_static(){
		$qry ="select * FROM gui where type = 'static' ORDER BY id_gui ASC";
		return $this->db->query($qry)->result();
	}
	public function read_gui_dyn(){
		$qry ="select a.*,b.status_high,b.status_low,b.divider FROM gui a left join im_mon b on a.alias = b.alias  where a.type = 'dynamic' GROUP BY a.id_gui ";
		return $this->db->query($qry)->result();
	}
	public function read_gui_group(){
		$qry ="select * FROM gui where type = 'group' ";
		return $this->db->query($qry)->result();
	}
	public function read_gui_child(){
		$qry ="select * FROM gui_group ";
		return $this->db->query($qry)->result();
	}

	public function del_gui(){
		$id_gui =$this->input->get('id_gui');
		$row=$this->Mon_model->del_gui($id_gui);
        if($row){
            echo json_encode(
                array(
                    'success' => true,
                    'message' => 'Update Success',
                
                )
            );
        }
        else {
            echo json_encode(
                array(
                    'success' => false,
                    'message' => 'Update Fail',
                
                )
            );
        };
	}

	public function upd_gui(){
		$x_axes=$this->input->get('x');
		$y_axes=$this->input->get('y');
		$id_gui =$this->input->get('id_gui');
		$row=$this->Mon_model->update_gui($x_axes,$y_axes,$id_gui);
        if($row){
            echo json_encode(
                array(
                    'success' => true,
                    'message' => 'Update Success',
                
                )
            );
        }
        else {
            echo json_encode(
                array(
                    'success' => false,
                    'message' => 'Update Fail',
                
                )
            );
        };
	}
	public function upd_size(){
		$w=$this->input->get('w');
		$id_gui =$this->input->get('id_gui');
		$row=$this->Mon_model->update_size($w,$id_gui);
        if($row){
            echo json_encode(
                array(
                    'success' => true,
                    'message' => 'Update Success',
                
                )
            );
        }
        else {
            echo json_encode(
                array(
                    'success' => false,
                    'message' => 'Update Fail',
                
                )
            );
        };
	}
	
	public function monitor_data(){
		$qry ="select * FROM im_mon a inner join device_list_perdevice b on a.id_device =b.id_device ORDER BY a.position  asc limit 8";
		return $this->db->query($qry)->result();
	}

	public function device_list_device(){
		$qry ="select a.*,b.gi_name as giname FROM device_list_perdevice a inner join device_list b on a.machine_code = b.machine_code ";
		return $this->db->query($qry)->result();
	}
	public function baca_data($user)
	{
		$this->db->select('*');
		$this->db->from('device_user') ;
	    $this->db->where('device_user.username', $user);
	    $this->db->join('data','data.machine_code = device_user.machine_code');
		$data = $this->db->get()->result();
		return $data;
	}

	public function baca_data_event($user)
	{
		// $this->db->select('*');
		// $this->db->from('device_user') ;
	    // $this->db->where('device_user.username', $user);
	    // $this->db->join('data_event','data_event.machine_code = device_user.machine_code');
		// $data = $this->db->get()->result();
		$qry ="SELECT a.*,a.lokasi as gi_name from data_event a inner join device_list b on a.machine_code = b.machine_code where b.gi_name like '%%' order by a.cur_date desc limit 6";
        return $this->db->query($qry)->result();
		//return $data;
	}
	public function disturbance_day($user)
	{
		date_default_timezone_set('Asia/Jakarta');
		$tanggal= date('Y-m-d');
		$this->db->select('no,machine_code,type,relay_id,lokasi,status,nama_file,tanggal,waktu');
		$this->db->from('device_user');
	    $this->db->where('device_user.username', $user);
		$this->db->where('data.tanggal', $tanggal);
	    $this->db->join('data','data.machine_code = device_user.machine_code');

	
		$num_results = $this->db->count_all_results();
		return $num_results;
	}

	public function event_day($user)
	{
		date_default_timezone_set('Asia/Jakarta');
		$tanggal= date('Y-m-d');
		$this->db->select('no,machine_code,type,relay_id,lokasi,status,nama_file,tanggal,waktu');
		$this->db->from('device_user');
	    $this->db->where('device_user.username', $user);
		$this->db->where('data.tanggal', $tanggal);
		// $this->db->where('data.type', "MICOM P123");
	    $this->db->join('data','data.machine_code = device_user.machine_code');

	
		$num_results = $this->db->count_all_results();
		return $num_results;
	}

		public function disturbance_device()
	{
		date_default_timezone_set('Asia/Jakarta');
		$tanggal= date('Y-m-d');
		$this->db->select('no,machine_code,relay_id,lokasi,status,nama_file,tanggal,waktu');
		$this->db->from('device_user');
	    // $this->db->where('device_user.machine_code', $user);
		$this->db->where('data.tanggal', $tanggal);
	    $this->db->join('data','data.machine_code = device_user.machine_code');

	
		$num_results = $this->db->count_all_results();
		return $num_results;
	}
    public function count_disturbance($user)
	{
			date_default_timezone_set('Asia/Jakarta');
		$tanggal= date('Y-m-d');
		$this->db->select('*, count(*) as jumlah');
		$this->db->from('device_user');
		//  $this->db->group_by('device_user.machine_code');
	    $this->db->group_by('data.machine_code');
	    $this->db->where('device_user.username', $user);
		  // $this->db->where('data.tanggal', $tanggal);
	    $this->db->join('data','data.machine_code = device_user.machine_code');
		// $this->db->group_by('data.machine_code');
		$data = $this->db->get()->result();
		return $data;
	}
	 public function count_event($user)
	{
		// 	date_default_timezone_set('Asia/Jakarta');
		// $tanggal= date('Y-m-d');
		// $this->db->select('*, count(*) as jumlah');
		// $this->db->from('device_user');
		// //  $this->db->group_by('device_user.machine_code');
	    // $this->db->group_by('data_event.machine_code');
	    // $this->db->where('device_user.username', $user);
		//   // $this->db->where('data.tanggal', $tanggal);
	    // $this->db->join('data_event','data_event.machine_code = device_user.machine_code');
		// // $this->db->group_by('data.machine_code');
		// $data = $this->db->get()->result();
		$qry ="SELECT count(*) as jumlah,b.* from device_user a inner join data_event b on a.machine_code = b.machine_code  WHERE a.machine_code = '0002' group by	b.machine_code   ";
        return $this->db->query($qry)->result();
		
	}
	public function user_notif($user)
	{

		$this->db->select('*');
		$this->db->from('device_user');
	    $this->db->where('device_user.machine_code', $user);
	    $this->db->join('notif','notif.machine_code = device_user.machine_code');


		// $this->db->select('chat_id');
		// $this->db->from('notif');
		// $this->db->where('machine_code', $id_product);
		$hasil = $this->db->count_all_results();
		return $hasil;
	}

	public function pie_chart($nim)
	{
		date_default_timezone_set('Asia/Jakarta');
		$tanggal= date('j/n/Y');
		$this->db->select('type, tanggal, count(*) as total');
		$this->db->group_by('month(tanggal)');
		$this->db->group_by('type');
		$this->db->from('data');
		$this->db->where('machine_code', $nim);
		// $this->db->where('tanggal', $tanggal);
		// $num_results = $this->db->count_all_results();
	  //   $this->db->where('k.id_thn_akad', $thn_akad);
	  //   $this->db->join('matakuliah as m','m.kode_matakuliah = k.kode_matakuliah');
		// $disturbance = $this->db->get()->result();
		//$hitung = $disturbance->num_rows();
		$dataPie = $this->db->get()->result();
		return $dataPie;
	}

	
	// Fungsi melakukan logout
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}

	
	
}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */
/* Please DO NOT modify this information : */
?>