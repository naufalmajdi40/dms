<?php

/* Copyright   : Amin Rusydi                             */
/* Hayooooooooooo Ngintip ajah nih.......               */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Krs
class Event extends CI_Controller
{
    // Konstruktor
	function __construct()
    {
        parent::__construct();
        $this->load->model('Event_model'); // Memanggil Krs_model yang terdapat pada models
		// $this->load->model('Tanaman_model'); // Memanggil Mahasiswa_model yang terdapat pada models
	    $this->load->model('Device_model');
		$this->load->model('Users_model'); // Memanggil Users_model yang terdapat pada models
		$this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
    }


   public function index(){
	// Jika session data username tidak ada maka akan dialihkan kehalaman login
	if (!isset($this->session->userdata['username'])) {
		redirect(base_url("login"));
	}

	$this->_rulesData(); // Rules atau aturan bahwa setiap form harus diisi

	// Menampilkan data berdasarkan id-nya yaitu username
	$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
	// $deviceUse = $this->Device_model->get_by_id($this->session->userdata['username']);
	$dataAdm = array(
			'username' => $rowAdm->username,
			'nama'     => $rowAdm->nama,
			'kebun'    => $rowAdm->kebun,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		// $id_product=$deviceUse->machine_code;
		$user=$rowAdm->username;

	$dataSensor=array(
				  
				   'back'   => site_url('Beranda'),
				   'sensor_data'=>$this->Event_model->baca_data($user),
	               'nim'=>$user,
			);

	$this->load->view('header',$dataAdm);	 // Menampilkan bagian header dan object data users
	$this->load->view('data/data_event',$dataSensor); // Menampilkan data Event
	$this->load->view('footer'); // Menampilkan bagian footer
	 
	}

	public function get_count_event(){
		$row= $this->Event_model->count_event()->result();
		echo json_encode($row);
	}

	//    public function count_event(){
    //     $qry ="select * as count_even from data_event where cur_date like CURDATE() ";
    //     return $this->db->query($qry);
    // }
	// public function json() {
    //     header('Content-Type: application/json');
    //     echo $this->Data_model->json();
    // }	



	// Fungsi rules atau aturan untuk pengisian pada form KRS
    public function _rulesData()
    {
	 $this->form_validation->set_rules('nim', 'nim', 'trim|required|min_length[10]|max_length[10]');
	//  $this->form_validation->set_rules('id_thn_akad','id_thn_akad', 'trim|required');
	}

	

}

?>