<?php

/* Copyright   : Amin Rusydi                             */
/* Hayooooooooooo Ngintip ajah nih.......               */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Krs
class Ultg extends CI_Controller
{
    // Konstruktor
	function __construct()
    {
        parent::__construct();
        $this->load->model('Data_model'); // Memanggil Krs_model yang terdapat pada models
		// $this->load->model('Tanaman_model'); // Memanggil Mahasiswa_model yang terdapat pada models
	  //  $this->load->model('Device_model');
		 $this->load->model('Ultg_model');
		$this->load->model('Users_model'); // Memanggil Users_model yang terdapat pada models
		$this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
    }


   public function index(){
	// Jika session data username tidak ada maka akan dialihkan kehalaman login
	if (!isset($this->session->userdata['username'])) {
		redirect(base_url("login"));
	}

	$this->_rulesData(); // Rules atau aturan bahwa setiap form harus diisi

	// Menampilkan data berdasarkan id-nya yaitu username
	$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
	// $deviceUse = $this->Device_model->get_by_id($this->session->userdata['username']);
	$dataAdm = array(
			'username' => $rowAdm->username,
			'nama'     => $rowAdm->nama,
			'kebun'    => $rowAdm->kebun,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		// $id_product=$deviceUse->machine_code;
		$user=$rowAdm->username;

	
 	$row['data']=$this->Ultg_model->get_device_ultg();
	$this->load->view('header',$dataAdm);	 // Menampilkan bagian header dan object data users
	$this->load->view('data/data_ultg',$row); // Menampilkan data ULTG
	$this->load->view('footer'); // Menampilkan bagian footer
	 
	}

	public function json() {
        header('Content-Type: application/json');
        echo $this->Data_model->json();
    }	


	// Fungsi membaca KRS berdasarkan NIM dan Tahun Akademik
	public function baca_data($user)
	{
	  // Jika session data username tidak ada maka akan dialihkan kehalaman login
	  if (!isset($this->session->userdata['username'])) {
		redirect(base_url("login"));
	  }

	  	$this->db->select('*');

		$this->db->from('device_user');
	    $this->db->where('device_user.username', $user);
	    $this->db->join('data','data.machine_code = device_user.machine_code');
		$this->db->join('device_list ','data.machine_code = device_list.machine_code');
		$data = $this->db->get()->result();
		return $data;

	//   $this->db->select('no,machine_code,relay_id,type,port_type,description,lokasi,status,nama_file,tanggal, waktu');
	//   $this->db->from('data');
	//   $this->db->where('machine_code', $nim);
	//   $this->db->order_by('no', 'asc');
	//   $this->db->order_by('waktu', 'asc');
	 
	// //   $this->db->where('k.id_thn_akad', $thn_akad);
	// //   $this->db->join('matakuliah as m','m.kode_matakuliah = k.kode_matakuliah');
	//   $data = $this->db->get()->result();
	// return $data;
	}


	//fungsi counting data disturbance record
	public function get_count_data(){
		$row= $this->Data_model->count_data()->result();
		echo json_encode($row);
	}

	

	// Fungsi rules atau aturan untuk pengisian pada form KRS
    public function _rulesData()
    {
	 $this->form_validation->set_rules('nim', 'nim', 'trim|required|min_length[10]|max_length[10]');
	//  $this->form_validation->set_rules('id_thn_akad','id_thn_akad', 'trim|required');
	}
	public function load_form_ultg(){
		$this->load->view('form_data/form_ultg');	
	}
	public function load_form_ultg_byid(){
		$id = $this->input->get('ultg_id');
		$sensor=array(
			
			'data_ultg'=>$this->Ultg_model->load_ultg_id($id)->result());
			
		//var_dump($sensor);
		$this->load->view('form_data/form_ultg',$sensor);
	}
	function save_ultg(){
		  $rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		 $data = array(
            'ultg_id'=>"ULTG".time(),
            'ultg_name' => $this->input->post('ultg_name'),
            'ultg_loc' => $this->input->post('ultg_loc'),
            'chat_id' => $this->input->post('chat_id'),        
	       );
		 $device = array(
             'machine_code' => $this->input->post('machine_code'),
            'username' =>$rowAdm->username
            
        );
 		$result = $this->Ultg_model->save_ultg($data,$device);

        if($result) {
            echo "Data berhasil disimpan";
        } else {
            echo "Gagal menyimpan data";
        }
	}
	function update_ultg(){
		$id = $this->input->post('ultg_id');
		
		 $data = array(
            'ultg_id' => $this->input->post('ultg_id'),
            'ultg_name' => $this->input->post('ultg_name'),
            'ultg_loc' => $this->input->post('ultg_loc'),
            'chat_id' => $this->input->post('chat_id'),
            
			// Tambahkan field lain sesuai kebutuhan
        );
 		$result = $this->Ultg_model->update($id,$data);
		
        if($result) {
            echo "Data berhasil disimpan";
        } else {
			print($id);
            echo "Gagal update data";
        }
	}
	function del_ultg(){
		
        $id = $this->input->post('ultg_id');
 		$result = $this->Ultg_model->delete($id);

        if($result) {
            echo "Delete Success";
        } else {
            echo "Delete Failed";
        }
	}
	

}

?>