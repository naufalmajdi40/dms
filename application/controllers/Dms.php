<?php

/* Copyright   : Amin Rusydi                             */
/* Hayooooooooooo Ngintip ajah nih.......               */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Krs
class Dms extends CI_Controller
{
    // Konstruktor
	function __construct()
    {
        parent::__construct();
        $this->load->model('Data_model'); // Memanggil Krs_model yang terdapat pada models
		// $this->load->model('Tanaman_model'); // Memanggil Mahasiswa_model yang terdapat pada models
	    $this->load->model('Device_model');
		$this->load->model('Users_model'); // Memanggil Users_model yang terdapat pada models
		$this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
    }


   public function index(){
	// Jika session data username tidak ada maka akan dialihkan kehalaman login
	if (!isset($this->session->userdata['username'])) {
		redirect(base_url("login"));
	}

	$this->_rulesData(); // Rules atau aturan bahwa setiap form harus diisi

	// Menampilkan data berdasarkan id-nya yaitu username
	$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
	// $deviceUse = $this->Device_model->get_by_id($this->session->userdata['username']);
	$dataAdm = array(
			'username' => $rowAdm->username,
			'nama'     => $rowAdm->nama,
			'kebun'    => $rowAdm->kebun,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		// $id_product=$deviceUse->machine_code;
		$user=$rowAdm->username;

	 $row['data']=$this->Device_model->get_device( $user);
 	
	$this->load->view('header',$dataAdm);	 // Menampilkan bagian header dan object data users
	$this->load->view('data/data_device',$row); // Menampilkan data ULTG
	$this->load->view('footer'); // Menampilkan bagian footer
	 
	}

	public function json() {
        header('Content-Type: application/json');
        echo $this->Data_model->json();
    }	


	// Fungsi membaca KRS berdasarkan NIM dan Tahun Akademik
	public function baca_data($user)
	{
	  // Jika session data username tidak ada maka akan dialihkan kehalaman login
	  if (!isset($this->session->userdata['username'])) {
		redirect(base_url("login"));
	  }

	  	$this->db->select('*');

		$this->db->from('device_user');
	    $this->db->where('device_user.username', $user);
	    $this->db->join('data','data.machine_code = device_user.machine_code');
		$this->db->join('device_list ','data.machine_code = device_list.machine_code');
		$data = $this->db->get()->result();
		return $data;

	//   $this->db->select('no,machine_code,relay_id,type,port_type,description,lokasi,status,nama_file,tanggal, waktu');
	//   $this->db->from('data');
	//   $this->db->where('machine_code', $nim);
	//   $this->db->order_by('no', 'asc');
	//   $this->db->order_by('waktu', 'asc');
	 
	// //   $this->db->where('k.id_thn_akad', $thn_akad);
	// //   $this->db->join('matakuliah as m','m.kode_matakuliah = k.kode_matakuliah');
	//   $data = $this->db->get()->result();
	// return $data;
	}


	//fungsi counting data disturbance record
	public function get_count_data(){
		$row= $this->Data_model->count_data()->result();
		echo json_encode($row);
	}

	//fungsi filter data
	public function filter_by_gi(){
		$gi = $this->input->get('gi');
		$row= $this->Data_model->filter_by_gi($gi)->result();
		echo json_encode($row);
	}
	public function filter_by_event(){
		$gi = $this->input->get('gi');
		$row= $this->Data_model->filter_by_event($gi)->result();
		echo json_encode($row);
	}

	// Fungsi rules atau aturan untuk pengisian pada form KRS
    public function _rulesData()
    {
	 $this->form_validation->set_rules('nim', 'nim', 'trim|required|min_length[10]|max_length[10]');
	//  $this->form_validation->set_rules('id_thn_akad','id_thn_akad', 'trim|required');
	}

	function del_dms(){
		
        $id = $this->input->post('id');
 		$result = $this->Device_model->del_dms($id);

        if($result) {
            echo "Delete Success";
        } else {
            echo "Delete Failed";
        }
	}
	function save_dms(){
		  $rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		 $data = array(
            'machine_code' => $this->input->post('machine_code'),
            'device_name' => $this->input->post('device_name'),
            'gi_name' => $this->input->post('gi_name'),
            'latitude' => $this->input->post('latitude'),
            'longitude' => $this->input->post('longitude'),
            'location' => $this->input->post('location'),
			'ultg_id' => $this->input->post('ultg_id'),
	 		//'location' =>$rowAdm->username,
	
		   'status_device'=>  1
        );
		 $device = array(
             'machine_code' => $this->input->post('machine_code'),
            'username' =>$rowAdm->username
            
        );
 		$result = $this->Device_model->save_dms($data,$device);

        if($result) {
            echo "Data berhasil disimpan";
        } else {
            echo "Gagal menyimpan data";
        }
	}
	function update_dms(){
		$id = $this->input->post('id');
		
		 $data = array(
            'machine_code' => $this->input->post('machine_code'),
            'device_name' => $this->input->post('device_name'),
            'gi_name' => $this->input->post('gi_name'),
            'latitude' => $this->input->post('latitude'),
            'longitude' => $this->input->post('longitude'),
            'location' => $this->input->post('location'),
			'ultg_id' => $this->input->post('ultg_id')
			// Tambahkan field lain sesuai kebutuhan
        );
 		$result = $this->Device_model->update($id,$data);
		
        if($result) {
            echo "Data berhasil disimpan";
        } else {
			print($id);
            echo "Gagal update data";
        }
	}
	function load_form_device(){
		$sensor=array(
			
			'data_ultg'=>$this->Device_model->load_ultg()->result());
		$this->load->view('form_data/form_device',$sensor);
		

	}
	function load_form_device_id(){
		$id = $this->input->get('id');
		$sensor=array(
			'data_dms'=>$this->Device_model->load_dms_id($id)->result(),
			'data_ultg'=>$this->Device_model->load_ultg()->result());
			
		//var_dump($sensor);
		$this->load->view('form_data/form_device',$sensor);
		

	}
	

}

?>