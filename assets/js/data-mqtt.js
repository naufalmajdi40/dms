//var host = "192.168.2.21";
//var host = "192.168.2.19";
//var host = window.location.hostname;
var host = "203.194.112.238";
var port = 8083;
//var userId = "mgi";
//var passwordId = "mgi123";
var userId = "das";
var passwordId = "mgi2022";

var coba = "80deg";
var grafikData;
var datacoba;
// /startConnect()

function startConnect() {
  var currentLocation = window.location.href;
  console.log(currentLocation);
  var lastSegment = currentLocation.split("/");

  console.log(lastSegment[lastSegment.length - 1]);
  //alert(lastSegment);
  clientID = "dms-" + parseInt(Math.random() * 100);
  client = new Paho.MQTT.Client(host, Number(port), clientID);
  $("#mdbusChartIA").attr("data-ratio", coba);
  client.onConnectionLost = onConnectionLost;
  client.onMessageArrived = onMessageArrived;

  client.connect({
    userName: userId,
    password: passwordId,
    onSuccess: onConnect,
  });
}

function onConnect() {
  let machine = $("#machine_code").text();
  topic = "";
  if (machine.length > 0) {
    topic = "DMS/" + machine + "/#";
  } else {
    //console.log(beranda);
    topic = "DMS/#";
  }
  // alert(machine.length);
  // alert(topic);
  // topic = "DMS/#";
  client.subscribe(topic);
  console.log("connected");
}

function onConnectionLost(responseObject) {
  setTimeout(startConnect(), 30000);
  console.warn(responseObject.errorMessage);
  //window.location.reload();
  //alert("disconnect")
  // document.getElementById("messages").innerHTML +=
  //     "<span> ERROR: Connection is lost.</span><br>";
  // if (responseObject != 0) {
  //     document.getElementById("messages").innerHTML +=
  //         "<span> ERROR:" + responseObject.errorMessage + "</span><br>";
  // }
}

function timeConverter(UNIX_timestamp) {
  let date = new Date(parseInt(UNIX_timestamp));
  var date_string = date.toLocaleString("en-GB");

  return date_string;
}

function removeBg(machine_code, id_device, alias) {
  $(`.sty_${machine_code}_${id_device}_${alias}`).removeClass("bg-green");
  $(`.sty_${machine_code}_${id_device}_${alias}`).removeClass("bg-red");
  $(`.sty_${machine_code}_${id_device}_${alias}`).removeClass("bg-yellow");
  $(`.icon_${machine_code}_${id_device}_${alias}`).removeClass("bg-green");
  $(`.icon_${machine_code}_${id_device}_${alias}`).removeClass("bg-red");
  $(`.icon_${machine_code}_${id_device}_${alias}`).removeClass("bg-yellow");
  $(`#dt_${machine_code}_${id_device}_${alias}`).removeClass("bg-navy-dark");

  $(`#dt_${machine_code}_${id_device}_${alias}`).removeClass("l-yellow");

  $(`#dt_${machine_code}_${id_device}_${alias}`).removeClass("l-second");
  $(`#dt_${machine_code}_${id_device}_${alias}`).removeClass("l-red");
}
function changeBg(kode, machine_code, id_device, alias) {
  removeBg(machine_code, id_device, alias);
  if (kode == 0) {
    // 0:merah
    // 1:hijau
    // 2:kuning
    $(`.sty_${machine_code}_${id_device}_${alias}`).addClass("bg-red");
    $(`.icon_${machine_code}_${id_device}_${alias}`).addClass("bg-red");
    $(`#dt_${machine_code}_${id_device}_${alias}`).addClass("l-red");
  } else if (kode == 1) {
    $(`.sty_${machine_code}_${id_device}_${alias}`).addClass("bg-green");
    $(`.icon_${machine_code}_${id_device}_${alias}`).addClass("bg-green");
    $(`#dt_${machine_code}_${id_device}_${alias}`).addClass("l-second");
  } else if (kode == 2) {
    $(`.sty_${machine_code}_${id_device}_${alias}`).addClass("bg-yellow");
    $(`.icon_${machine_code}_${id_device}_${alias}`).addClass("bg-yellow");
    $(`#dt_${machine_code}_${id_device}_${alias}`).addClass("bg-yellow");
  }
}

function onMessageArrived(message) {
  //alert("disconnect")
  //try(){
  dataMon = JSON.parse($("#monitorData").text());
  parsemsg = JSON.parse(message.payloadString);
  parseDest = message.destinationName.split("/");
  machine_code = parseDest[1];
  id_device = parseDest[3];
  item_id = parseDest[4];
  alias = parsemsg.alias;
  if (parsemsg.dataType == "float" || parsemsg.dataType == "integer") {
    let val = parseFloat(parsemsg.val);
    // try {
    //     if (val > 0) {
    //         console.log(val)
    //             //$(`.icon_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}`).removeClass("text-white")
    //         $(`.icon_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}`).addClass("text-yellow")
    //             // console.log(`ic_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}`)
    //     } else if (val == 0) {
    //         // $(`.icon_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}`).removeClass("text-yellow")
    //         $(`.icon_${buf[i].machine_code}_${buf[i].id_device}_${buf[i].alias}`).addClass("text-white")
    //     }
    // } catch (err) {}
    let max =
      parseFloat($(`#max_${machine_code}_${id_device}_${alias}`).text()) || 0;
    if (parsemsg.dataType == "float") {
      dvdr =
        parseInt($(`.dvdr_${machine_code}_${id_device}_${alias}`).text()) || 1;

      $(`#dtx_${machine_code}_${id_device}_${alias}`).text(
        (val / dvdr).toFixed(3)
      );
      $(`.dtg_${machine_code}_${id_device}_${alias}`).text(
        (val / dvdr).toFixed(3)
      );
      $(".comp_" + `${machine_code}_${id_device}_${alias}`).css(
        `width`,
        `${(val / dvdr).toFixed(3)}`
      );
      $(`.txtcomp_${machine_code}_${id_device}_${alias}`).text(
        `${(val / dvdr).toFixed(3)}`
      );
      // txtcomp_
    } else {
      val = parseInt(parsemsg.val);
      //
      $(`#dtx_${machine_code}_${id_device}_${alias}`).text(val);
      $(`.dtg_${machine_code}_${id_device}_${alias}`).text(val);
      //  $(`.txtcomp_${machine_code}_${id_device}_${alias}`).text(val.toFixed(0))
      $(`.txtcomp_${machine_code}_${id_device}_${alias}`).text(
        `${val.toFixed(3)}`
      );
      $(".comp_" + `${machine_code}_${id_device}_${alias}`).css(
        `width`,
        `${val.toFixed(3)}`
      );
    }
  } else if (parsemsg.dataType.toString().toLowerCase() == "boolean") {
    let val = parsemsg.val;

    $(`#dtx_${machine_code}_${id_device}_${alias}`).text(
      parsemsg.val.toUpperCase() == "FALSE"
        ? $(`.s_low_${machine_code}_${id_device}_${alias}`).text()
        : $(`.s_high_${machine_code}_${id_device}_${alias}`).text()
    );

    $(`.dtg_${machine_code}_${id_device}_${alias}`).text(
      parsemsg.val.toUpperCase() == "FALSE" ? "Normal" : "Fail"
    );

    // $(`.dtg_${machine_code}_${id_device}_${alias}`).text(parsemsg.val.toUpperCase())
    if (val.toLowerCase() == "true") {
      $(`.sty_${machine_code}_${id_device}_${alias}`).removeClass("bg-navy");
      $(`.sty_${machine_code}_${id_device}_${alias}`).removeClass("bg-green");
      $(`.sty_${machine_code}_${id_device}_${alias}`).addClass("bg-red");

      $(`.icon_${machine_code}_${id_device}_${alias}`).removeClass("bg-green");
      $(`.icon_${machine_code}_${id_device}_${alias}`).addClass("bg-red");
      $(`#dt_${machine_code}_${id_device}_${alias}`).removeClass("l-second");
      $(`#dt_${machine_code}_${id_device}_${alias}`).addClass("l-red");
    } else {
      $(`.sty_${machine_code}_${id_device}_${alias}`).removeClass("bg-navy");
      $(`.sty_${machine_code}_${id_device}_${alias}`).removeClass("bg-red");
      $(`.sty_${machine_code}_${id_device}_${alias}`).addClass("bg-green");

      $(`.icon_${machine_code}_${id_device}_${alias}`).removeClass("bg-red");
      $(`.icon_${machine_code}_${id_device}_${alias}`).addClass("bg-green");
      $(`#dt_${machine_code}_${id_device}_${alias}`).removeClass("l-red");
      $(`#dt_${machine_code}_${id_device}_${alias}`).addClass("l-second");
    }
  } else if (parsemsg.dataType == "bit-string") {
    let val = parsemsg.val;
    // 0:merah
    // 1:hijau
    // 2:kuning
    if (val == "0") {
      $(`#dtx_${machine_code}_${id_device}_${alias}`).text("Intermediete");
      $(`.dtg_${machine_code}_${id_device}_${alias}`).text("Intermediete");
      changeBg(2, machine_code, id_device, alias);
    } else if (val == "1") {
      $(`#dtx_${machine_code}_${id_device}_${alias}`).text("OFF");
      $(`.dtg_${machine_code}_${id_device}_${alias}`).text("OFF");
      changeBg(1, machine_code, id_device, alias);
    } else if (val == "2") {
      $(`#dtx_${machine_code}_${id_device}_${alias}`).text("ON");
      $(`.dtg_${machine_code}_${id_device}_${alias}`).text("ON");
      changeBg(0, machine_code, id_device, alias);
    } else if (val == "3") {
      $(`#dtx_${machine_code}_${id_device}_${alias}`).text("Bad State");
      $(`.dtg_${machine_code}_${id_device}_${alias}`).text("Bad State");
      changeBg(2, machine_code, id_device, alias);
    }

    //$(`#dtx_${machine_code}_${id_device}_${alias}`).text(val);
    //$(`.dtg_${machine_code}_${id_device}_${alias}`).text(val);
  } else if (parsemsg.dataType == "visible-string") {
    let val = parsemsg.val;

    $(`#dtx_${machine_code}_${id_device}_${alias}`).text(val);
    $(`.dtg_${machine_code}_${id_device}_${alias}`).text(val);
  } else if (parsemsg.dataType == "utc-time") {
    $(`#dtx_${machine_code}_${id_device}_${alias}`).removeClass(
      "info-box-number"
    );
    let val = timeConverter(parsemsg.val);
    $(`#dtx_${machine_code}_${id_device}_${alias}`).text(val);
    $(`.dtg_${machine_code}_${id_device}_${alias}`).text(val);
  }
  try {
    if (parsemsg.dataType == "float" || parsemsg.dataType == "integer") {
      localStorage.setItem(
        `${machine_code}_${id_device}_${alias}`,
        message.payloadString
      );
      arr = localStorage.getItem(`arr_${machine_code}_${id_device}_${alias}`);
      try {
        JsonArr = JSON.parse(arr);
        if (JsonArr.length < 30) {
          JsonArr.shift();
          JsonArr.push(parsemsg.val);
          localStorage.setItem(
            `arr_${machine_code}_${id_device}_${alias}`,
            JSON.stringify(JsonArr)
          );
        }
      } catch (error) {
        localStorage.setItem(
          `arr_${machine_code}_${id_device}_${alias}`,
          JSON.stringify([0])
        );
      }
    }
  } catch (err) {
    console.warn(err);
  }
}

function startDisconnect() {
  client.disconnect();
  document.getElementById("messages").innerHTML +=
    "<span> Disconnected. </span><br>";
}

function publishMessage() {
  msg = document.getElementById("Message").value;
  topic = document.getElementById("topic_p").value;

  Message = new Paho.MQTT.Message(msg);
  Message.destinationName = topic;

  client.send(Message);
  document.getElementById("messages").innerHTML +=
    "<span> Message to topic " + topic + " is sent </span><br>";
}
